package ukkf.glitcher;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageOutputStream;

import net.kroo.elliot.GifSequenceWriter;

public class JPEGGlitcher {
    private final byte[] buf;
    
    private final int iterationCount;
    
    private final Random rand;
    
    public JPEGGlitcher(byte[] buf, int iterationCount) {
    	this.buf = buf;
    	this.iterationCount = iterationCount;
    	this.rand = new Random();
    }
    
    public BufferedImage getImage() {
        byte[] buf = Arrays.copyOf(this.buf, this.buf.length);
        
        int jpegHeaderEnd = getJpegHeaderEnd(buf);
        
        for (int i = 0; i < iterationCount; i++) {
            glitchJpeg(buf, jpegHeaderEnd);
        }
        
        try {
            return ImageIO.read(new ByteArrayInputStream(buf));
        } catch (IOException e) {
            return getImage();
        }
    }
    
    private int getJpegHeaderEnd(byte[] buf) {
        for (int i = 0; i < buf.length; i++) {
            if (buf[i] == 0xFF && buf[i + 1] == 0xDA)
                return i + 2;
        }
        return 417;
    }
    
    private void glitchJpeg(byte[] buf, int headerEnd) {
        int len = buf.length - 4 - headerEnd;
        int loc = headerEnd + rand.nextInt(len);
        buf[loc] = (byte) rand.nextInt(256);
    }
    
    public void writeGIF(File file, int length) throws Exception {
        FileImageOutputStream out = new FileImageOutputStream(file);
        GifSequenceWriter writer = new GifSequenceWriter(out, BufferedImage.TYPE_INT_ARGB, 100, false);
        for (int i = 0; i < length; i++) {
            writer.writeToSequence(getImage());
            System.out.println(i + " / " + length); //$NON-NLS-1$
        }
        writer.close();
        out.close();
    }
}

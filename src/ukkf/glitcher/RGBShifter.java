package ukkf.glitcher;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ImageConsumer;
import java.awt.image.ImageProducer;
import java.io.File;
import java.util.Hashtable;
import java.util.Random;

import javax.imageio.stream.FileImageOutputStream;

import kdotjpg.OpenSimplexNoise;
import net.kroo.elliot.GifSequenceWriter;

public class RGBShifter implements ImageConsumer {
    public static final double MODIFIER = (double) 1 / 64;
    public static final int TYPE = BufferedImage.TYPE_INT_ARGB;
    
    private enum Status {
        WORKING, ABORTED, ERROR, FINISHED
    }
    
    private final double amount;
    private final ImageProducer prod;
    
    private int width;
    private int height;
    private ColorModel model;
    
    private Status status;
    
    private OpenSimplexNoise[][] noise;
    private int[][] r;
    private int[][] g;
    private int[][] b;
    private int[][] a;
    
    public RGBShifter(double amount, Image img) {
        this.amount = amount;
        this.prod = img.getSource();
        
        width = 0;
        height = 0;
        model = null;
        
        status = Status.WORKING;
        
        Random rand = new Random();
        noise = new OpenSimplexNoise[3][2];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                noise[i][j] = new OpenSimplexNoise(rand.nextLong());
            }
        }
        r = null;
        g = null;
        b = null;
        a = null;
    }
    
    public BufferedImage getImage() throws Exception {
        return getImage(0);
    }
    
    public BufferedImage getImage(double time) throws Exception {
        if (status == Status.WORKING) {
            throw new Exception("Still working");
        } else if (status == Status.ABORTED) {
            throw new Exception("Was aborted");
        } else if (status == Status.ERROR) {
            throw new Exception("An error occured");
        } else {
            BufferedImage image = new BufferedImage(width, height, TYPE);
            Graphics gr = image.getGraphics();
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    double[][] shifts = new double[3][2];
                    for (int i = 0; i < 3; i++) {
                        for (int j = 0; j < 2; j++) {
                            shifts[i][j] = (noise[i][j].eval(x * MODIFIER, y * MODIFIER, time)) * amount;
                        }
                    }
                    int r, g, b, a;
                    int xSrc, ySrc;
                    
                    xSrc = (int) (x + shifts[0][0]);
                    ySrc = (int) (y + shifts[0][1]);
                    if (xSrc >= 0 && ySrc >= 0 && xSrc < width && ySrc < height) {
                        r = this.r[xSrc][ySrc];
                        a = this.a[xSrc][ySrc];
                    } else {
                        r = 0;
                        a = 0;
                    }
                    
                    xSrc = (int) (x + shifts[1][0]);
                    ySrc = (int) (y + shifts[1][1]);
                    if (xSrc >= 0 && ySrc >= 0 && xSrc < width && ySrc < height) {
                        g = this.g[xSrc][ySrc];
                        a += this.a[xSrc][ySrc];
                    } else {
                        g = 0;
                    }
                    
                    xSrc = (int) (x + shifts[2][0]);
                    ySrc = (int) (y + shifts[2][1]);
                    if (xSrc >= 0 && ySrc >= 0 && xSrc < width && ySrc < height) {
                        b = this.b[xSrc][ySrc];
                        a += this.a[xSrc][ySrc];
                    } else {
                        b = 0;
                    }
                    
                    a /= 3;
                    
                    gr.setColor(new Color(r, g, b, a));
                    gr.fillRect(x, y, 1, 1);
                }
            }
            return image;
        }
    }
    
    @Override
    public void imageComplete(int status) {
        if ((status & ImageConsumer.IMAGEABORTED) == ImageConsumer.IMAGEABORTED) {
            this.status = Status.ABORTED;
        } else if ((status & ImageConsumer.IMAGEERROR) == ImageConsumer.IMAGEERROR) {
            this.status = Status.ERROR;
        } else {
            this.status = Status.FINISHED;
        }
        prod.removeConsumer(this);
    }
    
    private void prepareRaster() {
        r = new int[width][height];
        g = new int[width][height];
        b = new int[width][height];
        a = new int[width][height];
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                r[x][y] = 0;
                g[x][y] = 0;
                b[x][y] = 0;
                a[x][y] = 0;
            }
        }
    }
    
    @Override
    public void setColorModel(ColorModel model) {
        this.model = model;
        if (width != 0 && height != 0)
            prepareRaster();
    }
    
    @Override
    public void setDimensions(int width, int height) {
        this.width = (int) (width + Math.ceil(amount * 2));
        this.height = (int) (height + Math.ceil(amount * 2));
        if (model != null)
            prepareRaster();
    }
    
    @Override
    public void setHints(int hintflags) {}
    
    private void setPixel(double x, double y, int pixel, ColorModel model) {
        int r = model.getRed(pixel);
        int g = model.getGreen(pixel);
        int b = model.getBlue(pixel);
        int a = model.getAlpha(pixel);
        int xLoc = (int) (x + amount);
        int yLoc = (int) (y + amount);
        this.r[xLoc][yLoc] = r;
        this.g[xLoc][yLoc] = g;
        this.b[xLoc][yLoc] = b;
        this.a[xLoc][yLoc] = a;
    }
    
    @Override
    public void setPixels(final int x, final int y, int w, int h, ColorModel model, byte[] pixels, int off,
            int scansize) {
        System.out.println("x: " + x + " + " + w);
        System.out.println("y: " + y + " + " + h);
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                setPixel(i + x, j + y, pixels[off + i + j * scansize], model);
            }
        }
    }
    
    @Override
    public void setPixels(int x, int y, int w, int h, ColorModel model, int[] pixels, int off, int scansize) {
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                setPixel(i + x, j + y, pixels[off + i + j * scansize], model);
            }
        }
    }
    
    @Override
    public void setProperties(Hashtable<?, ?> props) {
        // ignore ?
        for (Object key : props.keySet()) {
            System.out.println(key.toString() + " : " + props.get(key).toString());
        }
    }
    
    public void start() {
        prod.startProduction(this);
    }
    
    public void writeGIF(File file, int length) throws Exception {
        FileImageOutputStream out = new FileImageOutputStream(file);
        GifSequenceWriter writer = new GifSequenceWriter(out, TYPE, 1, false);
        for (int i = 0; i < length; i++) {
            writer.writeToSequence(getImage(i * MODIFIER));
            System.out.println(i + " / " + length);
        }
        writer.close();
        out.close();
    }
}

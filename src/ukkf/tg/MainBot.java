package ukkf.tg;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.REST;

import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChatMembersCount;
import org.telegram.telegrambots.meta.api.methods.groupadministration.LeaveChat;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

import ukkf.tg.api.Action;
import ukkf.tg.api.Daemon;
import ukkf.tg.api.annot.Always;
import ukkf.tg.api.annot.Callback;
import ukkf.tg.api.annot.Command;
import ukkf.tg.dippy.Welcome;
import ukkf.tg.dippy.onall.Stats;
import ukkf.tg.util.ApiUtil;
import ukkf.tg.util.MiscUtil;
import ukkf.tg.util.NameUtil;
import ukkf.tg.util.SqlUtil;

public class MainBot extends TelegramLongPollingBot {
	public static boolean ignore = true;
	public static int start_time;

	private static ArrayList<Action> commands;
	private static ArrayList<Action> callbacks;
	private static ArrayList<Action> alwaysAct;
	private static Pattern botusername;

	private static Action welcome;
	private static Action stats;

	private static void loadActions() throws Exception {
		commands = new ArrayList<>();
		callbacks = new ArrayList<>();
		alwaysAct = new ArrayList<>();

		Scanner s = new Scanner(new FileInputStream("actions")); //$NON-NLS-1$
		ClassLoader loader = MainBot.class.getClassLoader();
		while (s.hasNextLine()) {
			String str = s.nextLine();
			if (str.charAt(0) == '#')
				continue;

			System.out.println("Loading " + str + " action..."); //$NON-NLS-1$ //$NON-NLS-2$
			var actClass = loader.loadClass(str);
			var annots = actClass.getAnnotations();
			var action = (Action) actClass.getDeclaredConstructor().newInstance();

			System.out.println("\tInitializing..."); //$NON-NLS-1$
			action.init();

			var tla = 0;
			var command = false;
			var callback = false;
			var onall = false;
			for (var annot : annots) {
				tla++;
				if (annot.annotationType().equals(Command.class))
					command = true;
				else if (annot.annotationType().equals(Callback.class))
					callback = true;
				else if (annot.annotationType().equals(Always.class))
					onall = true;
				else
					tla--;
			}

			if (tla == 0) {
				System.err.println("\tNo top level annots on action!"); //$NON-NLS-1$
				System.exit(1);
			} else if (tla > 1) {
				System.err.println("\tMore than one top level annot on action!"); //$NON-NLS-1$
				System.exit(1);
			} else if (command) {
				System.out.println("\t@Command"); //$NON-NLS-1$
				commands.add(action);
			} else if (callback) {
				System.out.println("\t@Callback"); //$NON-NLS-1$
				callbacks.add(action);
			} else if (onall) {
				System.out.println("\t@Always"); //$NON-NLS-1$
				alwaysAct.add(action);
			}
		}

		s.close();
		stats = new Stats();
		welcome = new Welcome();
		System.out.println("Initializing " + welcome.getClass().getName() + " module."); //$NON-NLS-1$ //$NON-NLS-2$
		welcome.init();
	}

	private static void loadDaemons(MainBot self) throws Exception {
		Scanner s = new Scanner(new FileInputStream("daemons")); //$NON-NLS-1$
		ClassLoader loader = MainBot.class.getClassLoader();
		while (s.hasNextLine()) {
			String str = s.nextLine();
			if (str.charAt(0) == '#')
				continue;

			System.out.println("Initializing " + str + " daemon."); //$NON-NLS-1$ //$NON-NLS-2$
			Daemon daemon = (Daemon) loader.loadClass(str).getDeclaredConstructor().newInstance();
			daemon.init(self);
			System.out.println("Starting it..."); //$NON-NLS-1$
			daemon.start();
		}

		s.close();
	}

	public static void main(String... args) {
		ApiContextInitializer.init();

		MiscUtil.flickr = new Flickr(BotConfig.FLICKR_APIKEY, BotConfig.FLICKR_SECRET, new REST());

		SqlUtil.init();

		ignore = true;
		start_time = (int) (new Date().getTime() / 1000) - 120;
		botusername = Pattern.compile("@[a-zA-Z0-9_]*bot"); //$NON-NLS-1$
		try {
			MainBot.loadActions();
		} catch (Exception e) {
			System.err.println("Error while loading actions"); //$NON-NLS-1$
			System.err.println(MiscUtil.exception(e));
			System.exit(0);
		}

		System.out.println("Starting bot"); //$NON-NLS-1$
		TelegramBotsApi api = new TelegramBotsApi();
		MainBot bot = new MainBot();
		try {
			api.registerBot(bot);
		} catch (TelegramApiRequestException e) {
			System.err.println("Error while registering the bot: "); //$NON-NLS-1$
			System.err.println(MiscUtil.exception(e));
			System.exit(0);
		}

		try {
			loadDaemons(bot);
		} catch (Exception e) {
			System.err.println("Error while loading daemons"); //$NON-NLS-1$
			System.err.println(MiscUtil.exception(e));
			System.exit(0);
		}
	}

	@Override
	public String getBotToken() {
		return BotConfig.BOT_TOKEN;
	}

	@Override
	public String getBotUsername() {
		return BotConfig.BOT_USERNAME;
	}

	@Override
	public void onClosing() {
		System.out.println("Closing..."); //$NON-NLS-1$
	}

	@Override
	public void onUpdateReceived(Update update) {
		try {
			if (update.hasMessage() || update.hasChannelPost()) {
				final Message msg;
				if (update.hasMessage())
					msg = update.getMessage();
				else
					msg = update.getChannelPost();

				if (msg.isGroupMessage() || msg.isSuperGroupMessage()) {
					if (msg.getLeftChatMember() != null && msg.getLeftChatMember().getId() == BotConfig.BOT_ID)
						return;

					var getCount = new GetChatMembersCount();
					getCount.setChatId(msg.getChatId());
					int count;
					try {
						count = this.execute(getCount);
					} catch (TelegramApiException e) {
						ApiUtil.notifyMe(String.format("Couldn't get member count for %s", msg.getChat().toString()), this); //$NON-NLS-1$
						return; // something's wrong if we get this
					}

					if (count > 1000 || (BotConfig.TESTING && msg.getChatId() != BotConfig.DEV_GROUP)) {
						try {
							var warn = new SendMessage();
							warn.setChatId(msg.getChatId());
							warn.setText(String.format("I'm sorry, but currently @%s doesn't support groups larger than 1000 members\n"
										+ "if you feel like your group should be an exception, please send a /devnote containing the group's username and/or join link and I'll consider",
										BotConfig.BOT_USERNAME));
							this.execute(warn);
						} catch (TelegramApiException e) {
							// IGNORE
						}

						var leave = new LeaveChat();
						leave.setChatId(msg.getChatId());
						if (this.execute(leave)) {
							ApiUtil.notifyMe(String.format("Just left uhh \"%s\", id:%d because they had %d members", msg.getChat().getTitle(), msg.getChatId(), count), this); //$NON-NLS-1$
						}
						return;
					}
				}


				if (msg.getDate() > start_time)
					ignore = false;
				else
					ignore = true;

				if ((msg.getNewChatMembers() != null && msg.getNewChatMembers().size() != 0)
						|| msg.getLeftChatMember() != null) {
					stats.exec(update, 0, null, this);
					welcome.exec(update, 0, null, this);
				}

				for (var act : alwaysAct) {
					act.exec(update, 0, null, this);
				}

				if (msg.hasText()) {
					String text;
					text = msg.getText();
					if (text.trim().isEmpty()) {
						ApiUtil.notifyMe("Empty message from " + NameUtil.getName(msg.getFrom()), this); //$NON-NLS-1$
						return;
					}

					text = text.replaceAll("@" + BotConfig.BOT_USERNAME, ""); //$NON-NLS-1$ //$NON-NLS-2$
					Matcher botty = botusername.matcher(text.split(" ")[0]); //$NON-NLS-1$
					boolean thisBot = !botty.find();
					String name = "WTF_IS_THIS_NAME"; //$NON-NLS-1$
					if (msg.getFrom() != null)
						name = NameUtil.getName(msg.getFrom());

					if (thisBot) {
						for (Action act : commands) {
							Pattern[] patterns = act.getPatterns();
							if (patterns != null) {
								int matched = -1;
								Matcher matcher = null;
								for (int i = 0; i < patterns.length; i++) {
									Pattern pattern = patterns[i];
									if (pattern.matcher(text).find()) {
										matched = i;
										matcher = pattern.matcher(text);
										matcher.find();
										break;
									}
								}

								if (matched == -1)
									continue;
								else {
									if (BotConfig.LOG_ON) {
										System.out.println(name + ": " + msg.getText() + "\nin " //$NON-NLS-1$ //$NON-NLS-2$
												+ NameUtil.getChatName(msg.getChat()));
									}

									// isn't a trigger
									if (!act.exec(update, matched, matcher, this))
										break;
								}
							} else {
								act.exec(update, 0, null, this);
							}
						}
					}
				}
			}
		} catch (Throwable e) {
			ApiUtil.notifyException(this, "", e); //$NON-NLS-1$
		}
	}
}

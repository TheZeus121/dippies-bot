package ukkf.tg.dippy;

import java.util.List;
import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.groupadministration.LeaveChat;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import ukkf.tg.BotConfig;
import ukkf.tg.MainBot;
import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.OnlyGroups;
import ukkf.tg.api.annot.Text;
import ukkf.tg.util.ApiUtil;
import ukkf.tg.util.NameUtil;
import ukkf.tg.util.SqlUtil;

// need some different pattern-ish annots
@OnlyGroups
@Text
@CanSkip
public class Welcome extends Action {
	@Override
	public void run(Update upd, Message msg, int which, Matcher matcher,
	                TelegramLongPollingBot bot)
			throws Exception {
		try {
			if (!SqlUtil.getBoolean(
						"groups", //$NON-NLS-1$
						msg.getChatId(),
						"welcome" //$NON-NLS-1$
					).orElse(true)) {
				return;
			}
		} catch (Exception e) {
			ApiUtil.notifyException(bot, "", e); //$NON-NLS-1$
			return;
		}
		long chat_id = msg.getChatId();

		if (msg.getLeftChatMember() == null) {
			List<User> news = msg.getNewChatMembers();
			SendMessage reply = new SendMessage();
			reply.setReplyToMessageId(msg.getMessageId());
			String reply_txt = "Hello"; //$NON-NLS-1$
			int new_membres = 0;
			for (User newy : news) {
				if (newy.getId() == BotConfig.BOT_ID) {
					reply.setText(
							"Hello " + msg.getChat().getTitle() + "\n"
							+ "I am a bot lol\n"
							+ "I can do all kinds of stuff, to find more about it use /help\n"
							+ "Remember to follow @dippies_bot_channel to find about downtimes and all kinds of other stuff."
					);
					sendMsg(reply, bot, chat_id);
					reply = new SendMessage();
					reply.setChatId(msg.getChatId().toString());
					reply.setReplyToMessageId(msg.getMessageId());
					ApiUtil.notifyMe("Added to " + NameUtil.getChatName(msg.getChat()), bot); //$NON-NLS-1$
				} else if (newy.getId() == BotConfig.BOT_OWNER) {
					reply.setText("It's the one and only..."); //$NON-NLS-1$
					sendMsg(reply, bot, chat_id);
					reply = new SendMessage();
					reply.setChatId(msg.getChatId().toString());
					reply.setReplyToMessageId(msg.getMessageId());
				} else if (SqlUtil.isGlobalAdmin(newy.getId()).orElse(false)) {
					reply_txt += ", " + NameUtil.getName(newy) + " :D"; //$NON-NLS-1$ //$NON-NLS-2$
					new_membres++;
				} else {
					reply_txt += ", " + NameUtil.getName(newy); //$NON-NLS-1$
					new_membres++;
				}

				if (BotConfig.LOG_ON)
					System.out.println("Welcome " + NameUtil.getName(newy)); //$NON-NLS-1$
			}

			reply_txt += "!"; //$NON-NLS-1$
			if (reply_txt.length() >= 4096) {
				reply_txt = "Hello!";
			}

			reply.setText(reply_txt);
			if (new_membres != 0)
				sendMsg(reply, bot, chat_id);
		} else {
			User ded = msg.getLeftChatMember();
			SendMessage reply = new SendMessage();
			if (ded.getId() == BotConfig.BOT_OWNER) {
				reply.setText("Do I need to leave as well?? >:("); //$NON-NLS-1$
			} else if (SqlUtil.isGlobalAdmin(ded.getId()).orElse(false)) {
				String txt = "Bye, " + NameUtil.getName(ded) + "! :(";
				if (txt.length() >= 4096) {
					txt = "Bye :(";
				}

				reply.setText(txt);
			} else {
				String txt = "Bye, " + NameUtil.getName(ded) + "!";
				if (txt.length() >= 4096) {
					txt = "Bye";
				}

				reply.setText(txt);
			}

			if (ded.getId() != BotConfig.BOT_ID) {
				reply.setChatId(msg.getChatId().toString());
				reply.setReplyToMessageId(msg.getMessageId());
				if (BotConfig.LOG_ON)
					System.out.println("Bye " + NameUtil.getName(ded)); //$NON-NLS-1$

				sendMsg(reply, bot, chat_id);
			} else {
				ApiUtil.notifyMe("Removed from " + NameUtil.getChatName(msg.getChat()), bot); //$NON-NLS-1$
			}
		}
	}

	private void sendMsg(SendMessage sm, TelegramLongPollingBot bot, long chat_id) {
		sm.setChatId(chat_id);
		try {
			if (!MainBot.ignore)
				bot.execute(sm);
		} catch (TelegramApiException e) {
			ApiUtil.notifyException(bot, "Failed to send welcome. Will leave this group now smh >:(", e); //$NON-NLS-1$

			LeaveChat leave = new LeaveChat();
			leave.setChatId(chat_id);
			try {
				bot.execute(leave);
			} catch (TelegramApiException e1) {
				ApiUtil.notifyException(bot, "OMG I can't even leave this fucking group...", e1); //$NON-NLS-1$
			}
		}
	}
}

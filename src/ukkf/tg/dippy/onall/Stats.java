package ukkf.tg.dippy.onall;

import java.sql.SQLException;
import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.Always;
import ukkf.tg.util.ApiUtil;
import ukkf.tg.util.SqlUtil;

@Always
public class Stats extends Action {
	@Override
	public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot) {
		try {
			if (msg.getFrom() != null) {
				int id = msg.getFrom().getId();
				SqlUtil.sql("select * from stats where id=" + id).ifPresent((rs) -> { //$NON-NLS-1$
					String name = SqlUtil.toSqlString(msg.getFrom().getFirstName());
					String last_name = SqlUtil.toSqlString(msg.getFrom().getLastName());
					String username = SqlUtil.toSqlString(msg.getFrom().getUserName());
					try {
						if (!rs.next()) {
							try {
								SqlUtil.sql(
										"insert into stats (id, name, last_name, username, msg_count, pat_count, hugging, huggers)\n" //$NON-NLS-1$
												+ "values (" + id + ", " + name + ", " + last_name + ", " + username //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
												+ ", 1, 0, 0, '{}');"); //$NON-NLS-1$
							} catch (Exception e1) {
								ApiUtil.notifyException(bot, "", e1); //$NON-NLS-1$
							}
						} else {
							SqlUtil.setString("stats", id, "name", name); //$NON-NLS-1$ //$NON-NLS-2$
							SqlUtil.setString("stats", id, "last_name", last_name); //$NON-NLS-1$ //$NON-NLS-2$
							SqlUtil.setString("stats", id, "username", username); //$NON-NLS-1$ //$NON-NLS-2$
							int msg_count = SqlUtil.getInt("stats", id, "msg_count").orElse(0); //$NON-NLS-1$ //$NON-NLS-2$
							msg_count++;
							SqlUtil.setInt("stats", id, "msg_count", msg_count); //$NON-NLS-1$ //$NON-NLS-2$
							SqlUtil.setDate("stats", id, "update"); //$NON-NLS-1$//$NON-NLS-2$
						}
					} catch (SQLException e) {
						ApiUtil.notifyException(bot, "", e); //$NON-NLS-1$
					}
				});
			}

			if (update.hasChannelPost() || msg.getChat().isGroupChat() || msg.getChat().isSuperGroupChat()) {
				long group_id = msg.getChatId();
				SqlUtil.sql("select * from groups where id=" + group_id).ifPresent((rs) -> { //$NON-NLS-1$
					try {
						if (!rs.next()) {
							try {
								SqlUtil.sql("insert into groups (id, trigger, welcome)\n" + "values (" + group_id //$NON-NLS-1$ //$NON-NLS-2$
										+ ", true, true);"); //$NON-NLS-1$
							} catch (Exception e1) {
								ApiUtil.notifyException(bot, "", e1); //$NON-NLS-1$
							}
						} else {
							SqlUtil.setDate("groups", group_id, "update"); //$NON-NLS-1$ //$NON-NLS-2$
						}
					} catch (SQLException e) {
						ApiUtil.notifyException(bot, "", e); //$NON-NLS-1$
					}
				});
			}
		} catch (SQLException e) {
			ApiUtil.notifyException(bot, "", e); //$NON-NLS-1$
		}
	}
}

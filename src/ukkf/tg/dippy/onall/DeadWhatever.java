package ukkf.tg.dippy.onall;

import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.Always;

@Always
public class DeadWhatever extends Action {
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        if (msg.getChatId().equals(-1001057427061l)) {
            SendMessage reply = new SendMessage();
            reply.setText(messages.getString("dead_group") + " :D"); //$NON-NLS-1$ //$NON-NLS-2$
            reply.setChatId(msg.getChatId());
            reply.setReplyToMessageId(msg.getMessageId());
            bot.execute(reply);
        }
    }
    
}

package ukkf.tg.dippy.daemons;

import java.sql.Date;
import java.sql.ResultSet;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;

import ukkf.tg.api.Daemon;
import ukkf.tg.api.annot.SleepTime;
import ukkf.tg.util.ApiUtil;
import ukkf.tg.util.SqlUtil;

@SleepTime(1000 * 60 * 60) // 1 hour
public class DbCleaner extends Daemon {
    private static final int MAX_SIZE = 9000; // not 10000 because i might create some static small tables
    
    @Override
    public void run(TelegramLongPollingBot bot) throws Exception {
        int count;
        int userCount;
        int groupCount;
        
        ResultSet rs = SqlUtil.sql("SELECT COUNT(id) FROM stats").get(); //$NON-NLS-1$
        if (rs.next())
            userCount = rs.getInt(1);
        else
            throw new Exception("WAT (stats)"); //$NON-NLS-1$
        
        rs = SqlUtil.sql("SELECT COUNT(id) FROM groups").get(); //$NON-NLS-1$
        if (rs.next())
            groupCount = rs.getInt(1);
        else
            throw new Exception("WAT (groups)"); //$NON-NLS-1$
        
        count = userCount + groupCount;
        
        if (count < MAX_SIZE) return; // in compliance
        
        // we're close to reaching the limit, we gotta clean up
        
        int oldCount = count;
        int oldUserCount = userCount;
        int oldGroupCount = groupCount;
        
        Date date = new Date(Instant.now().minus(10, ChronoUnit.DAYS).toEpochMilli());
        
        SqlUtil.sql("DELETE FROM stats WHERE update<?", date); //$NON-NLS-1$
        SqlUtil.sql("DELETE FROM groups WHERE update<?", date); //$NON-NLS-1$
        
        rs = SqlUtil.sql("SELECT COUNT(id) FROM stats").get(); //$NON-NLS-1$
        if (rs.next())
            userCount = rs.getInt(1);
        else
            throw new Exception("WAT (stats)"); //$NON-NLS-1$
        
        rs = SqlUtil.sql("SELECT COUNT(id) FROM groups").get(); //$NON-NLS-1$
        if (rs.next())
            groupCount = rs.getInt(1);
        else
            throw new Exception("WAT (groups)"); //$NON-NLS-1$
        
        count = userCount + groupCount;
        
        StringBuilder sb = new StringBuilder();
        sb.append("Cleanup complete\n\n"); //$NON-NLS-1$
        
        sb.append(oldUserCount);
        sb.append(" -> "); //$NON-NLS-1$
        sb.append(userCount);
        sb.append(" users\n"); //$NON-NLS-1$
        
        sb.append(oldGroupCount);
        sb.append(" -> "); //$NON-NLS-1$
        sb.append(groupCount);
        sb.append(" groups\n"); //$NON-NLS-1$
        
        sb.append(oldCount);
        sb.append(" -> "); //$NON-NLS-1$
        sb.append(count);
        sb.append(" total rows\n"); //$NON-NLS-1$
        
        ApiUtil.notifyMe(sb.toString(), bot);
    }
}

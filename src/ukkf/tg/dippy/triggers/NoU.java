package ukkf.tg.dippy.triggers;

import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendSticker;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Trigger;

@Command
@Regex("n+o+\\s+u+")
@Trigger
@CanSkip
public class NoU extends Action {
    @Override
    public void run(Update upd, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot) throws Exception {
        SendSticker reply = new SendSticker();
        reply.setSticker("CAADBAADcQQAAlBIwAXdt837UKGyWAI"); //$NON-NLS-1$
        reply.setReplyToMessageId(msg.getMessageId());
        reply.setChatId(msg.getChatId());
        bot.execute(reply);
    }
    
}

package ukkf.tg.dippy.triggers;

import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendAudio;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.Audio;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Trigger;

@Command
@Regex("h+a+i+l+i+e+")
@Audio
@Trigger
@CanSkip
public class Hailie extends Action {
    @Override
    public void run(Update upd, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot) throws Exception {
        SendAudio reply = new SendAudio();
        reply.setAudio("CQADBAADpgEAAk_vAAFQlwABV524e9l4Ag"); //$NON-NLS-1$
        reply.setPerformer("Eminem"); //$NON-NLS-1$
        reply.setTitle("Doe Rae Me (Hailie's Revenge) ft D12, Obie Trice"); //$NON-NLS-1$
        reply.setCaption("Don't ever say my little girl's name in a song!"); //$NON-NLS-1$
        reply.setReplyToMessageId(msg.getMessageId());
        reply.setChatId(msg.getChatId());
        bot.execute(reply);
    }
    
}

package ukkf.tg.dippy.triggers;

import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.MustSign;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;
import ukkf.tg.api.annot.Trigger;
import ukkf.tg.util.SqlUtil;

@Command
@Regex({
    "(;|')\\s*((-+(\\s|-)*)|(_+(\\s|_)*)|(~+(\\s|~)*))\\s*\\1", // ;-; ;_; ;~; '~'
    /* ; OR ' - - OR _ _ OR ~ ~ ; OR ' */
    "('|@|>|;|:)\\s*(\\^|,-+|-+,)\\s*\\1", // '-' ;^; etc
    /* (' OR @ OR > OR ; OR :)  ^ OR ,- OR -,  \1 */
    "\\b(X|O|0)\\s*(\\^|n|,-+|-+,)\\s*\\1\\b", // ono X^X etc
    /* (X OR O OR 0)  ^ OR N OR ,- OR -,  \1 */
    "(:|=|(\\bX+))\\s*('|\"|-|\\s)*\\s*(\\(|(C+\\b)|<|\\[|\\{)", // :C XC :( :-( :'( :"( :'-( etc
    /* : OR = OR X  ' " -  ( OR C OR < OR [ OR { */
    "((\\bD+)|\\)|3|>|\\]|\\})\\s*('|\"|-|\\s)*\\s*(:|=|(X+\\b))", // D: DX ): )-: )': etc
    /* D OR ) OR 3 OR > OR ] OR }  ' " -  : OR = OR X */
    "\\b(Q+|U+)\\s*\\.+\\s*(Q+|U+)\\b", // Q.Q
    /* Q or U . Q or U */
    "\\(\\(", // ((
    /* ( ( */
})
@Text
@MustSign
@Trigger
public class AutoPat extends Action {
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        int pats;
        int id = msg.getFrom().getId();
        pats = SqlUtil.getInt("stats", id, "pat_count").orElse(0); //$NON-NLS-1$ //$NON-NLS-2$
        if (pats > 999)
            pats = 999;
        pats++;
        SqlUtil.setInt("stats", id, "pat_count", pats); //$NON-NLS-1$//$NON-NLS-2$
        
        SendMessage reply = new SendMessage();
        reply.setText("Aww, don't be sad.\nHere, a /pat from me :)"); //$NON-NLS-1$
        reply.setChatId(msg.getChatId());
        reply.setReplyToMessageId(msg.getMessageId());
        bot.execute(reply);
    }
}

// TODO:
// .v.
// .y.
// \:
// /:
// :^(
// 8(
// :L
// :Z
// J:
// +(

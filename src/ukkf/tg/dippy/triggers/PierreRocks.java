package ukkf.tg.dippy.triggers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;

@Command
@Regex("(pierre +[a-z]+s)|(/pierresucks)")
@Text
@InlineCommand
@CanSkip
public class PierreRocks extends Action {
    
    private Pattern verb_pattern, command_pattern;
    
    @Override
    public void init() {
        super.init();
        verb_pattern = Pattern.compile("pierre +([a-z]+s)"); //$NON-NLS-1$
        command_pattern = Pattern.compile("/pierresucks"); //$NON-NLS-1$
    }
    
    @Override
    public void run(Update upd, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot) throws Exception {
        SendMessage reply = new SendMessage();
        reply.setChatId(msg.getChatId());
        reply.setReplyToMessageId(msg.getMessageId());
        String msg_text = msg.getText();
        if (command_pattern.matcher(msg_text.toLowerCase()).find()) {
            reply.setText("You suck more."); //$NON-NLS-1$
        } else {
            Matcher m = verb_pattern.matcher(msg_text.toLowerCase());
            m.find();
            String verb = m.group(1);
            if (verb.endsWith("sses") || verb.endsWith("xes") || verb.endsWith("ches") || verb.endsWith("shes") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
                    || verb.endsWith("oes")) //$NON-NLS-1$
                verb = verb.substring(0, verb.length() - 2);
            else if (verb.endsWith("ies")) //$NON-NLS-1$
                verb = verb.substring(0, verb.length() - 3) + "y"; //$NON-NLS-1$
            else
                verb = verb.substring(0, verb.length() - 1);
            reply.setText("You " + verb + " more."); //$NON-NLS-1$ //$NON-NLS-2$
        }
        bot.execute(reply);
    }
    
}

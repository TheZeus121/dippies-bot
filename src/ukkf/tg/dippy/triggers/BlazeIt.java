package ukkf.tg.dippy.triggers;

import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendAudio;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.Document;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Trigger;

@Command
@Regex("(4+2+0+)|(s+m+o+k+e+\\s+w+ee+d+\\s+e+v+e+r+y+d+a+y+)")
@Document
@Trigger
@CanSkip
public class BlazeIt extends Action {
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        if (rand.nextBoolean()) {
            SendDocument reply = new SendDocument();
            reply.setDocument("CgADBAADKwEAArkE1VHWixi41XXGzQI"); //$NON-NLS-1$
            reply.setCaption(messages.getString("thats_right")); //$NON-NLS-1$
            reply.setReplyToMessageId(msg.getMessageId());
            reply.setChatId(msg.getChatId());
            bot.execute(reply);
        } else {
            SendAudio reply = new SendAudio();
            reply.setAudio("CQADAgADJgkAAlOWgElAuY0Rqk8GXwI"); //$NON-NLS-1$
            reply.setCaption(messages.getString("thats_right")); //$NON-NLS-1$
            reply.setPerformer("Snoop Lion"); //$NON-NLS-1$
            reply.setTitle("Smoke the Weed"); //$NON-NLS-1$
            reply.setReplyToMessageId(msg.getMessageId());
            reply.setChatId(msg.getChatId());
            bot.execute(reply);
        }
    }
}

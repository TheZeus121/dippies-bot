package ukkf.tg.dippy.triggers;

import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;
import ukkf.tg.api.annot.Trigger;

@Command
@Regex("^[A-Za-z]+ me$")
@Text
@Trigger
@CanSkip
public class VerbMe extends Action {
    
    @Override
    public void run(Update upd, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot) throws Exception {
        String verb = msg.getText().split(" ")[0].toLowerCase(); //$NON-NLS-1$
        if (verb.equals("have")) //$NON-NLS-1$
            verb = "has"; //$NON-NLS-1$
        else if (verb.equals("be")) //$NON-NLS-1$
            verb = "is"; //$NON-NLS-1$
        else if (verb.endsWith("z") || verb.endsWith("s") || verb.endsWith("x") || verb.endsWith("ch") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
                || verb.endsWith("sh") || (verb.endsWith("o") && !verb.endsWith("oo"))) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            verb += "es"; //$NON-NLS-1$
        else if (verb.endsWith("y") && !(verb.endsWith("ey") || verb.endsWith("uy") || verb.endsWith("iy") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
                || verb.endsWith("oy") || verb.endsWith("ay"))) //$NON-NLS-1$ //$NON-NLS-2$
            verb = verb.substring(0, verb.length() - 1) + "ies"; //$NON-NLS-1$
        else
            verb += "s"; //$NON-NLS-1$
        SendMessage reply = new SendMessage();
        reply.setText("* " + verb + " you *"); //$NON-NLS-1$ //$NON-NLS-2$
        reply.setChatId(msg.getChatId());
        reply.setReplyToMessageId(msg.getMessageId());
        bot.execute(reply);
    }
    
}

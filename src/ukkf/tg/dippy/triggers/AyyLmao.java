package ukkf.tg.dippy.triggers;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;
import ukkf.tg.api.annot.Trigger;
import ukkf.tg.util.SqlUtil;

@Command
@Regex({
    "(a+yy+)",
    "(l+m+a+o+)",
    "(a+l+i+e+n+)",
    "(l+(a+l+)+)",
    "(?:^|[^t])(r+i+p+)($|[^e])"
})
@Text
@Trigger
@CanSkip
public class AyyLmao extends Action {
    private String response(int length, int number, TelegramLongPollingBot bot, Message msg) throws Exception {
        int count = SqlUtil.getInt("triggers", number, "count").orElse(0); //$NON-NLS-1$ //$NON-NLS-2$
        count++;
        SqlUtil.setInt("triggers", number, "count", count); //$NON-NLS-1$ //$NON-NLS-2$
        if (count % 100 == 0) {
            SendMessage gratz = new SendMessage();
            gratz.setChatId(msg.getChatId());
            gratz.setReplyToMessageId(msg.getMessageId());
            Object[] args = {
                count, SqlUtil.getString("triggers", number, "trigger").orElse("") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            };
            format.applyPattern(messages.getString("congratulations")); //$NON-NLS-1$
            gratz.setText(format.format(args));
            bot.execute(gratz);
        }
        String ret;
        switch (number) {
            case 0: // AYY
                ret = "lmao"; //$NON-NLS-1$
                for (int i = 0; i < length - 3; i++)
                    ret += "o"; //$NON-NLS-1$
                return ret;
            case 1: // LMAO
                ret = "ayy"; //$NON-NLS-1$
                for (int i = 0; i < length - 4; i++)
                    ret += "y"; //$NON-NLS-1$
                return ret;
            case 2: // ALIEN
                length -= 5;
                ret = "ayy"; //$NON-NLS-1$
                for (int i = 0; i < length / 2; i++)
                    ret += "y"; //$NON-NLS-1$
                ret += " lmao"; //$NON-NLS-1$
                length -= length / 2;
                for (int i = 0; i < length; i++) {
                    ret += "o"; //$NON-NLS-1$
                }
                return ret;
            case 3: // SEI
                ret = "QUACK"; //$NON-NLS-1$
                for (int i = 0; i < (length - 3) / 3; i++)
                    ret += " QUACK"; //$NON-NLS-1$
                return ret;
            case 4: // RIP
                ret = "in pieces"; //$NON-NLS-1$
                for (int i = 0; i < length - 3; i++)
                    ret += "s"; //$NON-NLS-1$
                return ret;
            default:
                return "ERROR BZZT BZZT"; //$NON-NLS-1$
        }
    }
    
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        String text = msg.getText().toLowerCase();
        SendMessage reply = new SendMessage();
        reply.setChatId(msg.getChatId().toString());
        reply.setReplyToMessageId(msg.getMessageId());
        
        ArrayList<Matcher> matchers = new ArrayList<>();
        for (Pattern pattern : patterns) {
            matchers.add(pattern.matcher(text));
        }
        
        ArrayList<Boolean> bools = new ArrayList<>();
        for (Matcher m : matchers)
            bools.add(m.find());
        
        int counter = 0;
        ArrayList<Integer> starts = new ArrayList<>();
        for (int i = 0; i < bools.size(); i++)
            if (bools.get(i)) {
                starts.add(matchers.get(i).start(1));
                counter++;
            } else
                starts.add(Integer.MAX_VALUE);
        
        ArrayList<Integer> lengths = new ArrayList<>();
        for (int i = 0; i < bools.size(); i++)
            if (bools.get(i))
                lengths.add(matchers.get(i).end(1) - starts.get(i));
            else
                lengths.add(0);
        
        String ret = ""; //$NON-NLS-1$
        while (counter > 0) {
            int min = Integer.MAX_VALUE;
            int min_id = -1;
            for (int i = 0; i < starts.size(); i++) {
                if (starts.get(i) < min) {
                    min = starts.get(i);
                    min_id = i;
                }
            }
            starts.set(min_id, Integer.MAX_VALUE);
            counter--;
            if (min_id != -1) {
                ret += response(lengths.get(min_id), min_id, bot, msg);
                if (counter > 0)
                    ret += " "; //$NON-NLS-1$
            }
        }
        reply.setText(ret);
        bot.execute(reply);
    }
}

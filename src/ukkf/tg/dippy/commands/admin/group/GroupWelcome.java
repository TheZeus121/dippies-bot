package ukkf.tg.dippy.commands.admin.group;

import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.GroupAdmin;
import ukkf.tg.api.annot.OnlyGroups;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;
import ukkf.tg.util.SqlUtil;

@Command
@Regex("^/groupwelc")
@Text
@OnlyGroups
@GroupAdmin
public class GroupWelcome extends Action {
    @Override
    public void run(Update upd, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot) throws Exception {
        String[] tokens = msg.getText().split(" "); //$NON-NLS-1$
        SendMessage reply = new SendMessage();
        reply.setChatId(msg.getChatId());
        if (tokens.length < 2 || !(tokens[1].equalsIgnoreCase("on") || tokens[1].equalsIgnoreCase("off"))) //$NON-NLS-1$ //$NON-NLS-2$
        {
            reply.setText(messages.getString("help.groupwelc")); //$NON-NLS-1$
            reply.enableMarkdown(true);
        } else {
            SqlUtil.setBoolean("groups", msg.getChatId(), "welcome", tokens[1].equalsIgnoreCase("on")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            reply.setText(messages.getString("settings_changed") + " :)"); //$NON-NLS-1$ //$NON-NLS-2$
        }
        bot.execute(reply);
    }
    
}

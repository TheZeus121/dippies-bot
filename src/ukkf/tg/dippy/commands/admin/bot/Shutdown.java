package ukkf.tg.dippy.commands.admin.bot;

import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.GlobalAdmin;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.MustSign;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;

@Command
@Regex("/shutdown")
@Text
@InlineCommand
@MustSign
@GlobalAdmin
public class Shutdown extends Action {
    
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        SendMessage reply = new SendMessage();
        reply.setText(messages.getString("shutdown") + " :("); //$NON-NLS-1$ //$NON-NLS-2$
        reply.setChatId(msg.getChatId().toString());
        reply.setReplyToMessageId(msg.getMessageId());
        bot.execute(reply);
        System.exit(0);
    }
}

package ukkf.tg.dippy.commands.admin.bot;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChat;
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChatMembersCount;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.GlobalAdmin;
import ukkf.tg.api.annot.MustSign;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;
import ukkf.tg.util.ApiUtil;
import ukkf.tg.util.SqlUtil;

@Command
@Regex("^/listgroups")
@Text
@MustSign
@GlobalAdmin
public class ListGroups extends Action {
	private class ChatStruct {
		public Chat chat;
		public int member_count;
	}

	private class ChatComp implements Comparator<ChatStruct> {
		@Override
		public int compare(ChatStruct chat1, ChatStruct chat2) {
			if (chat1 == chat2)
				return 0;

			if (chat1 == null)
				return 1;
			else if (chat2 == null)
				return -1;

			return chat2.member_count - chat1.member_count;
		}
	}

	@Override
	public void run(Update update, Message msg, int which, Matcher matcher,
	                TelegramLongPollingBot bot)
			throws Exception {
		final int length;
		length = SqlUtil.getSize("groups", "id") //$NON-NLS-1$ //$NON-NLS-2$
		                .orElse(-1);

		final SendMessage sm = new SendMessage();
		sm.setChatId(msg.getChatId());
		sm.setReplyToMessageId(msg.getMessageId());
		if (length == -1) {
			sm.setText("Couldn't get group count :(");
		} else {
			final StringBuilder sb = new StringBuilder();
			sb.append("Currently there are ");
			sb.append(length);
			sb.append(" groups.\n");
			sb.append("I'm assuming it will take me at least ");
			if (length == 0) {
				sb.append('0');
			} else if (length % 10 == 0) {
				sb.append(length / 2 - 5);
			} else {
				sb.append((length - length % 10) / 2);
			}
			sb.append(" minutes.\n\n");
			sb.append("TODO: actually take note of how much time passes.\n");
			sm.setText(sb.toString());
		}
		bot.execute(sm);

		if (length == -1)
			return;

		new Thread(() -> runThread(msg, bot)).start();
	}

	private void runThread(Message msg, TelegramLongPollingBot bot) {
		try {
			final long startingTime = System.currentTimeMillis();
			final ResultSet rs;
				rs = SqlUtil.sql("SELECT id FROM groups") //$NON-NLS-1$
							.orElse(null);

			if (rs == null) {
				ApiUtil.notifyMe(
					"Couldn't get list of all groups :(", //$NON-NLS-1$
					bot
				);
				return;
			}

			PriorityQueue<ChatStruct> q;
			/* Why use 11 as default capacity? It was put that way on the Java
			 * docs lol
			 */
			q = new PriorityQueue<>(11, new ChatComp());

			while (rs.next()) {
				final long id = rs.getLong(1);
				final GetChatMembersCount gcmc = new GetChatMembersCount();
				gcmc.setChatId(id);
				final GetChat gc = new GetChat();
				gc.setChatId(id);
				final ChatStruct chat = new ChatStruct();
				chat.member_count = -1;
				try {
					chat.member_count = bot.execute(gcmc);
					chat.chat = bot.execute(gc);
				} catch (Exception e) {
					/* Possibly spam protection, so sleep for 5 min */
					try {
						Thread.sleep(5 * 60 * 1000);
					} catch (InterruptedException e1) {}

					try {
						if (chat.member_count == -1)
							chat.member_count = bot.execute(gcmc);

						chat.chat = bot.execute(gc);
					} catch (TelegramApiException e1) {
						/* Most likely bot is removed from chat, attempt to
						 * remove the group from the database.
						 */
						try {
							final StringBuilder sb = new StringBuilder();
							sb.append(
								"DELETE FROM groups WHERE id=" //$NON-NLS-1$
							);
							sb.append(id);
							SqlUtil.sql(sb.toString());
							/* Ignore the exception because this isn't THAT
							 * important.
							 */
						} catch (SQLException e2) {}
						continue;
					}
				}

				q.add(chat);
			}

			final File file = File.createTempFile(
				"dip", ".txt" //$NON-NLS-1$ //$NON-NLS-2$
			);
			file.deleteOnExit();

			final PrintStream out;
			out = new PrintStream(new FileOutputStream(file));
			for (final ChatStruct chat : q) {
				final StringBuilder sb = new StringBuilder();
				sb.append(chat.chat.getTitle());
				sb.append("\n> Members: "); //$NON-NLS-1$
				sb.append(chat.member_count);
				sb.append("\n> ID: "); //$NON-NLS-1$
				sb.append(chat.chat.getId());
				out.println(sb.toString());
			}
			out.close();

			final SendDocument sd = new SendDocument();
			sd.setChatId(msg.getChatId());
			sd.setReplyToMessageId(msg.getMessageId());
			sd.setDocument(file);

			final StringBuilder sb = new StringBuilder();
			sb.append("Time elapsed: ");
			sb.append((System.currentTimeMillis() - startingTime) / 60_000);
			sb.append(" minutes.");

			sd.setCaption(sb.toString());

			try {
				bot.execute(sd);
				file.delete();
			} catch (TelegramApiException e) {
				final SendMessage sm = new SendMessage();
				sm.setChatId(msg.getChatId());
				sm.setReplyToMessageId(msg.getMessageId());

				sb.setLength(0);
				sb.append("Failed to send file. File name: ");
				sb.append(file.getAbsolutePath());

				sm.setText(sb.toString());

				try {
					bot.execute(sm);
				} catch (TelegramApiException e1) {
					ApiUtil.notifyException(bot, sb.toString(), e1);
				}
			}
		} catch (SQLException | IOException e) {
			ApiUtil.notifyException(bot, "", e); //$NON-NLS-1$

			final SendMessage sm = new SendMessage();
			sm.setChatId(msg.getChatId());
			sm.setReplyToMessageId(msg.getMessageId());
			sm.setText("Something went wrong..."); //$NON-NLS-1$
			try {
				bot.execute(sm);
			} catch (Exception e1) {
				ApiUtil.notifyException(bot, "", e); //$NON-NLS-1$
			}
		}
	}
}

package ukkf.tg.dippy.commands.admin.bot;

import java.io.CharArrayReader;
import java.io.CharArrayWriter;
import java.util.regex.Matcher;

import javax.script.ScriptEngineManager;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.GlobalAdmin;
import ukkf.tg.api.annot.MustSign;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;

@Command
@Regex("^/(bsh|js) (.+)")
@Text
@CanSkip
@MustSign
@GlobalAdmin
public class Eval extends Action {
	@Override
	public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
			throws Exception {
		var name = matcher.group(1);
		var engine = new ScriptEngineManager().getEngineByName(name);
		if (engine == null) {
			var reply = new SendMessage();
			reply.setText("Couldn't find `" + name + "' engine");
			reply.setChatId(msg.getChatId());
			reply.setReplyToMessageId(msg.getMessageId());
			bot.execute(reply);
			return;
		}

		var code = matcher.group(2);
		var ctx = engine.getContext();

		var wr = new CharArrayWriter();
		ctx.setWriter(wr);
		ctx.setErrorWriter(wr);
		ctx.setReader(new CharArrayReader(new char[0]));

		engine.setContext(ctx);

		var ret = engine.eval(code);

		var sb = new StringBuilder();
		sb.append(wr.toString());
		if (ret != null) {
			sb.append("\n\n"); //$NON-NLS-1$
			sb.append(ret.toString());
		}

		var reply = new SendMessage();
		reply.setText(sb.toString());
		reply.setChatId(msg.getChatId());
		reply.setReplyToMessageId(msg.getMessageId());
		bot.execute(reply);
	}
}
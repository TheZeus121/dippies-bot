package ukkf.tg.dippy.commands.admin.bot;

import java.io.File;
import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.Document;
import ukkf.tg.api.annot.GlobalAdmin;
import ukkf.tg.api.annot.MustSign;
import ukkf.tg.api.annot.Regex;

@Command
@Regex("^/dl\\s+(.*)$")
@Document
@MustSign
@GlobalAdmin
public class Download extends Action {
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        File ret = new File(matcher.group(1));
        SendDocument reply = new SendDocument();
        reply.setChatId(msg.getChatId());
        reply.setReplyToMessageId(msg.getMessageId());
        reply.setDocument(ret);
        reply.setCaption(matcher.group(1));
        bot.execute(reply);
    }
}

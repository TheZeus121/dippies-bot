package ukkf.tg.dippy.commands.admin.bot;

import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.GlobalAdmin;
import ukkf.tg.api.annot.MustSign;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;
import ukkf.tg.util.SqlUtil;

@Command
@Regex("^/sql\\s(.*)$")
@Text
@MustSign
@GlobalAdmin
public class Sql extends Action {
	@Override
	public void run(Update upd, Message msg, int which, Matcher m, TelegramLongPollingBot bot) throws Exception {
		SendMessage reply = new SendMessage();
		reply.setChatId(msg.getChatId());
		reply.setReplyToMessageId(msg.getMessageId());
		reply.setText(SqlUtil.sqlString(m.group(1)));
		bot.execute(reply);
	}
	
}

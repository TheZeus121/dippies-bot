package ukkf.tg.dippy.commands.admin.bot;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.net.whois.WhoisClient;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.GlobalAdmin;
import ukkf.tg.api.annot.MustSign;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;
import ukkf.tg.util.NameUtil;
import ukkf.tg.util.SqlUtil;

@Command
@Regex("^/whois .*$")
@Text
@CanSkip
@MustSign
@GlobalAdmin
public class Whois extends Action {
    
    static private Pattern server;
    
    @Override
    public void init() {
        super.init();
        server = Pattern.compile("WHOIS Server: (.*)"); //$NON-NLS-1$
    }
    
    @Override
    public void run(Update upd, Message msg, int which, Matcher matcherarg, TelegramLongPollingBot bot)
            throws Exception {
        String[] tokens = msg.getText().split(" "); //$NON-NLS-1$
        int id;
        SendMessage reply = new SendMessage();
        reply.setChatId(msg.getChatId());
        reply.setReplyToMessageId(msg.getMessageId());
        if (tokens[1].startsWith("@")) //$NON-NLS-1$
        {
            id = SqlUtil.queryId("stats", tokens[1].substring(1)).orElse(0); //$NON-NLS-1$
        } else if (Character.digit(tokens[1].charAt(0), 10) != -1) {
            id = Integer.parseInt(tokens[1]);
        } else {
            String whois_server = WhoisClient.DEFAULT_HOST;
            String result = "who the fuck knows"; //$NON-NLS-1$
            reply.setText(result);
            do {
                whois.connect(whois_server);
                try {
                    result = whois.query(tokens[1]);
                } catch (Exception e) {
                    result = ""; //$NON-NLS-1$
                }
                if (!result.trim().equals("") && !result.trim().startsWith("No")) //$NON-NLS-1$ //$NON-NLS-2$
                    reply.setText(result);
                Matcher matcher = server.matcher(result);
                whois_server = ""; //$NON-NLS-1$
                if (matcher.find())
                    whois_server = matcher.group(1);
            } while (!whois_server.trim().equals("")); //$NON-NLS-1$
            reply.disableWebPagePreview();
            bot.execute(reply);
            return;
        }
        if (id == 0)
            reply.setText(messages.getString("unknown_person") + " :/"); //$NON-NLS-1$//$NON-NLS-2$
        else
            reply.setText(NameUtil.getNameLongUnknown(id, bot));
        bot.execute(reply);
    }
    
}

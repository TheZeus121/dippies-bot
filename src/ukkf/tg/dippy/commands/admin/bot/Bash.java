package ukkf.tg.dippy.commands.admin.bot;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.GlobalAdmin;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.MustSign;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;
import ukkf.tg.util.ApiUtil;

@Command
@Regex({
    "^\\$(.*)", "/env"
})
@Text
@InlineCommand
@MustSign
@GlobalAdmin
public class Bash extends Action {
    
    private String readFile(File file, TelegramLongPollingBot bot) {
        byte[] buf;
        try {
            buf = Files.readAllBytes(file.toPath());
        } catch (IOException e) {
            ApiUtil.notifyException(bot, "", e); //$NON-NLS-1$
            return "Some error happened and I lost all logs ;-;";
        }
        return new String(buf);
    }
    
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        if (which == 1) { // /env
            StringBuilder sb = new StringBuilder();
            Map<String, String> env = System.getenv();
            for (String key : env.keySet()) {
                sb.append(key);
                sb.append(" = "); //$NON-NLS-1$
                sb.append(env.get(key));
                sb.append("\n\n"); //$NON-NLS-1$
            }
            SendMessage reply = new SendMessage();
            reply.setChatId(msg.getChatId());
            reply.setReplyToMessageId(msg.getMessageId());
            reply.setText(sb.toString());
            bot.execute(reply);
            return;
        }
        
        String text = matcher.group(1);
        String[] cmd = text.trim().split("\\s"); //$NON-NLS-1$
        ArrayList<String> cmdList = new ArrayList<>();
        for (String str : cmd) {
            if (str != null && str.trim().length() != 0) {
                cmdList.add(str.trim());
            }
        }
        cmd = new String[cmdList.size()];
        cmd = cmdList.toArray(cmd);
        
        ProcessBuilder pb = new ProcessBuilder(cmd);
        
        // set working dir
        pb.directory(new File("/")); //$NON-NLS-1$
        
        File log = File.createTempFile("dip", ".txt"); //$NON-NLS-1$//$NON-NLS-2$
        log.deleteOnExit();
        
        pb.redirectInput(new File("/dev/urandom")); //$NON-NLS-1$
        pb.redirectErrorStream(true);
        pb.redirectOutput(log);
        
        Process proc = pb.start();
        
        Thread t1 = new Thread(() -> {
            // give 10 seconds for process to finish
            try {
                proc.waitFor(10, TimeUnit.SECONDS);
            } catch (InterruptedException e) {}
            
            // if process is still alive, keep on killing it
            while (proc.isAlive()) {
                proc.destroyForcibly();
                try {
                    proc.waitFor();
                } catch (InterruptedException e) {}
            }
            
            // now that all the other threads are dead we can send the message
            String logs;
            int status = proc.exitValue();
            try {
                if (Files.size(log.toPath()) > 4000) {
                    logs = null;
                } else {
                    logs = readFile(log, bot);
                    if (status != 0)
                        logs += "\n\n\n--> " + Integer.toString(status); //$NON-NLS-1$
                    log.delete();
                }
            } catch (IOException e) {
                logs = "Some error happened and I lost all logs ;-;";
                ApiUtil.notifyException(bot, "", e); //$NON-NLS-1$
            }
            
            if (logs == null || logs.length() > 4096) {
                InputFile inputFile = new InputFile();
                inputFile.setMedia(log, "log.txt"); //$NON-NLS-1$

                SendDocument reply = new SendDocument();
                reply.setChatId(msg.getChatId());
                reply.setReplyToMessageId(msg.getMessageId());
                reply.setDocument(inputFile);
                if (status != 0)
                    reply.setCaption("--> " + Integer.toString(status) + " :("); //$NON-NLS-1$ //$NON-NLS-2$
                try {
                    bot.execute(reply);
                    log.delete();
                } catch (TelegramApiException e) {
                    ApiUtil.notifyException(bot, "", e); //$NON-NLS-1$
                }
            } else {
                SendMessage reply = new SendMessage();
                reply.setChatId(msg.getChatId());
                reply.setReplyToMessageId(msg.getMessageId());
                reply.setText(logs);
                try {
                    bot.execute(reply);
                } catch (TelegramApiException e) {
                    ApiUtil.notifyException(bot, "", e); //$NON-NLS-1$
                }
            }
        }, "Process executor"); //$NON-NLS-1$
        t1.start();
    }
    
}

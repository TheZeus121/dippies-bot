package ukkf.tg.dippy.commands.user;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import ukkf.tg.BotConfig;
import ukkf.tg.api.Action;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.MustSign;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;
import ukkf.tg.util.NameUtil;
import ukkf.tg.util.SqlUtil;

@Command
@Regex("/hug")
@Text
@InlineCommand
@MustSign
public class Hug extends Action {
    @Override
    public void run(Update upd, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot) throws Exception {
        User hugger = msg.getFrom();
        SendMessage reply = new SendMessage();
        reply.setChatId(msg.getChatId());
        User hugged = null;
        if (msg.getReplyToMessage() != null) {
            hugged = msg.getReplyToMessage().getFrom();
        }
        if (hugged == null) {
            reply.setReplyToMessageId(msg.getMessageId());
            InlineKeyboardMarkup ikm = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> map = new ArrayList<>();
            List<InlineKeyboardButton> line = new ArrayList<>();
            map.add(line);
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(messages.getString("get_help")); //$NON-NLS-1$
            button.setUrl("http://telegram.me/dippies_bot?start=XDhelp-hug"); //$NON-NLS-1$
            line.add(button);
            ikm.setKeyboard(map);
            reply.setReplyMarkup(ikm);
            reply.setText(messages.getString("hug_nothing")); //$NON-NLS-1$
        } else if (hugged.getId() == BotConfig.BOT_ID) {
            reply.setReplyToMessageId(msg.getMessageId());
            reply.setText(messages.getString("hug_bot")); //$NON-NLS-1$
        } else if (hugged.getId().equals(hugger.getId())) {
            reply.setReplyToMessageId(msg.getMessageId());
            reply.setText(messages.getString("hug_self")); //$NON-NLS-1$
        } else {
            ArrayList<Integer> huggers;
            Integer hugging;
            int r_id = hugger.getId();
            int v_id = hugged.getId();
            hugging = SqlUtil.getInt("stats", r_id, "hugging").orElse(0); //$NON-NLS-1$ //$NON-NLS-2$
            Integer[] tmp = {};
            tmp = SqlUtil.<Integer>getArray("stats", v_id, "huggers").orElse(new Integer[0]); //$NON-NLS-1$ //$NON-NLS-2$
            huggers = new ArrayList<>(Arrays.asList(tmp));
            format.applyPattern(messages.getString("hug_started")); //$NON-NLS-1$
            String text = format.format(new Object[]{
                NameUtil.getName(hugger)
            }) + "\n"; //$NON-NLS-1$
            if (hugging != null && hugging != 0) {
                ArrayList<Integer> lol;
                try {
                    lol = new ArrayList<>(Arrays
                            .asList(SqlUtil.<Integer>getArray("stats", hugging, "huggers").orElse(new Integer[0]))); //$NON-NLS-1$ //$NON-NLS-2$
                    for (int i = 0; i < lol.size() && i >= 0; i++) {
                        if (lol.get(i).equals(hugger.getId())) {
                            lol.remove(i);
                            i--;
                        }
                    }
                    format.applyPattern(messages.getString("hug_ended")); //$NON-NLS-1$
                    text += format.format(new Object[]{
                        NameUtil.getName(hugger), NameUtil.getNameKnown(hugging)
                    }) + "\n"; //$NON-NLS-1$
                    SqlUtil.setArray("stats", hugging, "huggers", lol); //$NON-NLS-1$ //$NON-NLS-2$
                } catch (NullPointerException e) {
                    // since I delete data in SQL (thanks Heroku) this code ^ could throw
                    // NullPointerException
                }
            }
            if (huggers.size() != 0) {
                format.applyPattern(messages.getString("hug_also")); //$NON-NLS-1$
                String tmp_s = ""; //$NON-NLS-1$
                for (int i = 0; i < huggers.size(); i++) {
                    tmp_s += NameUtil.getNameKnown(huggers.get(i));
                    if (i < huggers.size() - 1)
                        tmp_s += ", "; //$NON-NLS-1$
                }
                text += format.format(new Object[]{
                    tmp_s
                }) + "\n"; //$NON-NLS-1$
            }
            hugging = hugged.getId();
            for (int i = 0; i < huggers.size(); i++)
                if (huggers.get(i).equals(hugger.getId())) {
                    huggers.remove(i);
                    i--;
                }
            huggers.add(hugger.getId());
            SqlUtil.setInt("stats", r_id, "hugging", hugging); //$NON-NLS-1$ //$NON-NLS-2$
            SqlUtil.setArray("stats", v_id, "huggers", huggers); //$NON-NLS-1$ //$NON-NLS-2$
            reply.setText(text);
            reply.setReplyToMessageId(msg.getReplyToMessage().getMessageId());
        }
        bot.execute(reply);
    }
    
}

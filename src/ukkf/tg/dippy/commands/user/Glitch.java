package ukkf.tg.dippy.commands.user;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;

import javax.imageio.ImageIO;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.GetFile;
import org.telegram.telegrambots.meta.api.methods.send.SendAnimation;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.send.SendSticker;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.PhotoSize;
import org.telegram.telegrambots.meta.api.objects.Update;

import io.humble.video.Codec;
import io.humble.video.Decoder;
import io.humble.video.Demuxer;
import io.humble.video.Encoder;
import io.humble.video.MediaDescriptor;
import io.humble.video.MediaPacket;
import io.humble.video.MediaPicture;
import io.humble.video.Muxer;
import io.humble.video.MuxerFormat;
import io.humble.video.awt.MediaPictureConverter;
import io.humble.video.awt.MediaPictureConverterFactory;
import ukkf.glitcher.JPEGGlitcher;
import ukkf.glitcher.RGBShifter;
import ukkf.tg.BotConfig;
import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.Image;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.MustReply;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.util.ApiUtil;
import ukkf.tg.util.FileUtil;
import ukkf.tg.util.MiscUtil;

@Command
@Regex("/glitch")
@Image
@InlineCommand
@CanSkip
@MustReply
public class Glitch extends Action {
    private final static int JPEG_QUANTITY = 2;
    private final static int RGB_QUANTITY = 20;
    
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        Message imgMsg = msg.getReplyToMessage();
        if (!imgMsg.hasPhoto() && imgMsg.getSticker() == null && !MiscUtil.hasGIF(imgMsg)) {
            SendMessage reply = new SendMessage();
            reply.setChatId(msg.getChatId());
            reply.setReplyToMessageId(msg.getMessageId());
            reply.setText("You can only use this command on images, stickers, and GIFs!");
            bot.execute(reply);
            return;
        }
        final String fileId;
        final String extension;
        final String outExtension;
        if (imgMsg.hasPhoto()) {
            PhotoSize photo = MiscUtil.getLargest(imgMsg.getPhoto());
            fileId = photo.getFileId();
            extension = "jpg"; //$NON-NLS-1$
            outExtension = "png"; //$NON-NLS-1$
        } else if (imgMsg.getSticker() != null) { // sticker
            fileId = imgMsg.getSticker().getFileId();
            extension = "webp"; //$NON-NLS-1$
            outExtension = "webp"; //$NON-NLS-1$
        } else { // GIF
            fileId = imgMsg.getDocument().getFileId();
            extension = "mp4"; //$NON-NLS-1$
            outExtension = "mp4"; //$NON-NLS-1$
        }
        GetFile get = new GetFile();
        get.setFileId(fileId);
        org.telegram.telegrambots.meta.api.objects.File apiFile = bot.execute(get);
        String url = apiFile.getFileUrl(BotConfig.BOT_TOKEN);
        File file = FileUtil.downloadToNewFile(url, extension);
        
        if (extension.equals("mp4")) { //$NON-NLS-1$
            new Thread(() -> runMP4Glitcher(file, bot, msg)).start();
            return;
        }
        
        BufferedImage img = ImageIO.read(file);
        file.delete();
        RGBShifter shifter = new RGBShifter(RGB_QUANTITY, img);
        new Thread(() -> {
            shifter.start();
            try {
                BufferedImage image = shifter.getImage();
                File imageFile = File.createTempFile("dip", "." + outExtension); //$NON-NLS-1$//$NON-NLS-2$
                imageFile.deleteOnExit();
                ImageIO.write(image, outExtension, imageFile);
                if (imgMsg.hasPhoto()) {
                    SendPhoto reply = new SendPhoto();
                    reply.setChatId(msg.getChatId());
                    reply.setPhoto(imageFile);
                    bot.execute(reply);
                } else {
                    SendSticker reply = new SendSticker();
                    reply.setChatId(msg.getChatId());
                    reply.setSticker(imageFile);
                    bot.execute(reply);
                }
            } catch (Exception e) {
                ApiUtil.notifyException(bot, "", e); //$NON-NLS-1$
            }
        }).start();
    }
    
    private void runMP4Glitcher(File file, TelegramLongPollingBot bot, Message msg) {
        File ret = null;
        try {
            final var demuxer = Demuxer.make();
            
            demuxer.open(file.getAbsolutePath(), null, false, true, null, null);
            int numStreams = demuxer.getNumStreams();
            int videoStreamId = -1;
            Decoder decoder = null;
            for (int i = 0; i < numStreams; i++) {
                final var stream = demuxer.getStream(i);
                final var d = stream.getDecoder();
                if (d != null && d.getCodecType() == MediaDescriptor.Type.MEDIA_VIDEO) {
                    videoStreamId = i;
                    decoder = d;
                    break;
                }
            }
            if (decoder == null)
                throw new Exception("Couldn't find a video stream in " + file.getAbsolutePath()); //$NON-NLS-1$
            
            decoder.open(null, null);
            
            ret = File.createTempFile("dip", ".mp4"); //$NON-NLS-1$ //$NON-NLS-2$
            ret.deleteOnExit();
            
            final var muxer = Muxer.make(ret.getAbsolutePath(), null, null);
            final var codec = Codec.findEncodingCodec(muxer.getFormat().getDefaultVideoCodecId());
            final var encoder = Encoder.make(codec);
            
            encoder.setWidth(decoder.getWidth());
            encoder.setHeight(decoder.getHeight());
            encoder.setPixelFormat(decoder.getPixelFormat());
            encoder.setTimeBase(decoder.getTimeBase());
            if (muxer.getFormat().getFlag(MuxerFormat.Flag.GLOBAL_HEADER)) {
            	encoder.setFlag(Encoder.Flag.FLAG_GLOBAL_HEADER, true);
            }
            
            encoder.open(null, null);
            muxer.addNewStream(encoder);
            
            muxer.open(null, null);
            
            final var inPic = MediaPicture.make(
            		decoder.getWidth(),
            		decoder.getHeight(),
            		decoder.getPixelFormat());
            
            final var converter = MediaPictureConverterFactory.createConverter(
            		MediaPictureConverterFactory.HUMBLE_BGR_24, inPic);
            
            final var in = MediaPacket.make();
            final var out = MediaPacket.make();
          
            final var scale = encoder.getTimeBase().getDouble();
            
            while (demuxer.read(in) >= 0) {
                if (in.getStreamIndex() != videoStreamId)
                    continue;
                
                int offset = 0;
                int bytesRead = 0;
                do {
                    bytesRead += decoder.decode(inPic, in, offset);
                    if (inPic.isComplete()) {
                    	var outPic = processPicture(converter, inPic, scale);
                    	writePicture(muxer, encoder, out, outPic);
                    }
                    
                    offset += bytesRead;
                } while (offset < in.getSize());
            }
            
            do {
                decoder.decode(inPic, null, 0);
                if (inPic.isComplete()) {
                	var outPic = processPicture(converter, inPic, scale);
                	writePicture(muxer, encoder, out, outPic);
                }
            } while (inPic.isComplete());
            
            demuxer.close();
            
            do {
            	encoder.encode(out, null);
            	if (out.isComplete()) {
            		muxer.write(out, false);
            	}
            } while (out.isComplete());
            
            muxer.close();
            
            var reply = new SendAnimation();
            reply.setChatId(msg.getChatId());
            reply.setReplyToMessageId(msg.getMessageId());
            reply.setAnimation(ret);
            bot.execute(reply);
        } catch (Exception e) {
            ApiUtil.notifyException(bot, "", e); //$NON-NLS-1$
        } finally {
            if (ret != null)
                ret.delete();
            file.delete();
        }
    }
    
    private MediaPicture processPicture(MediaPictureConverter conv, MediaPicture inPic, double scale) throws IOException {
    	var buf = new ByteArrayOutputStream();
        var img = conv.toImage(null, inPic);
        ImageIO.write(img, "jpg", buf); //$NON-NLS-1$
        var glitcher = new JPEGGlitcher(buf.toByteArray(), JPEG_QUANTITY);
        img = glitcher.getImage();
        
        var outPic = conv.toPicture(null, img, 0);
        var actualTimeStamp =
        		inPic.getTimeStamp() * scale * inPic.getTimeBase().getDouble();
        outPic.setTimeStamp(inPic.getPts());
        
        System.out.println(inPic.getPts());
        
        return outPic;
    }
    
    private void writePicture(Muxer muxer, Encoder encoder, MediaPacket packet, MediaPicture pic) {
    	do {
    		encoder.encode(packet, pic);
    		if (packet.isComplete()) {
    			muxer.write(packet, false);
    		}
    	} while (packet.isComplete());
    }
}

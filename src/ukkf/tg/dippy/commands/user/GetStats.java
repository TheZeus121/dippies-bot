package ukkf.tg.dippy.commands.user;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.MustSign;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;
import ukkf.tg.util.NameUtil;
import ukkf.tg.util.SqlUtil;

@Command
@Regex({
    "/stats", "/karma", "/status([^/]|$)"
})
@Text
@InlineCommand
@CanSkip
@MustSign
public class GetStats extends Action {
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        User from = msg.getFrom();
        int id = from.getId();
        String name = NameUtil.getName(from);
        SendMessage reply = new SendMessage();
        reply.setChatId(msg.getChatId().toString());
        reply.setReplyToMessageId(msg.getMessageId());
        if (which == 0) {
            Object[] args = {
                name
            };
            format.applyPattern(messages.getString("information")); //$NON-NLS-1$
            String s = format.format(args);
            format.applyPattern(messages.getString("msg_count")); //$NON-NLS-1$
            args = new Object[]{
                SqlUtil.getInt("stats", id, "msg_count").orElse(0) //$NON-NLS-1$ //$NON-NLS-2$
            };
            reply.setText(s + "\n" + format.format(args)); //$NON-NLS-1$
        } else if (which == 1) {
            Object[] args = {
                name
            };
            format.applyPattern(messages.getString("information")); //$NON-NLS-1$
            String s = format.format(args);
            args = new Object[]{
                SqlUtil.getInt("stats", id, "pat_count").orElse(0) //$NON-NLS-1$ //$NON-NLS-2$
            };
            format.applyPattern(messages.getString("karma")); //$NON-NLS-1$
            reply.setText(s + "\n" + format.format(args)); //$NON-NLS-1$
        } else if (which == 2) {
            Integer hugging;
            hugging = SqlUtil.getInt("stats", id, "hugging").orElse(0); //$NON-NLS-1$ //$NON-NLS-2$
            List<Integer> huggers;
            new ArrayList<>();
            Integer[] tmp = SqlUtil.<Integer>getArray("stats", id, "huggers").orElse(new Integer[0]); //$NON-NLS-1$ //$NON-NLS-2$
            huggers = new ArrayList<Integer>(Arrays.asList(tmp));
            String text = ""; //$NON-NLS-1$
            if (hugging == null || hugging.equals(from.getId()) || hugging.equals(0)) {
                format.applyPattern(messages.getString("hugging_nothing")); //$NON-NLS-1$
                text += format.format(new Object[]{
                    name
                }) + "\n"; //$NON-NLS-1$
            } else {
                format.applyPattern(messages.getString("hugging")); //$NON-NLS-1$
                text += format.format(new Object[]{
                    name, NameUtil.getNameKnown(hugging)
                }) + "\n"; //$NON-NLS-1$
            }
            if (huggers.isEmpty()) {
                text += messages.getString("hugged_nothing") + ":(\n"; //$NON-NLS-1$ //$NON-NLS-2$
            } else {
                text += messages.getString("hugged") + " "; //$NON-NLS-1$ //$NON-NLS-2$
                for (int i = 0; i < huggers.size(); i++) {
                    if (huggers.get(i) == null || huggers.get(i).equals(from.getId()) || huggers.get(i).equals(0)) {
                        huggers.remove(i);
                        i--;
                    }
                    if (i > 0)
                        text += ", "; //$NON-NLS-1$
                    text += NameUtil.getNameKnown(huggers.get(i));
                }
                text += ".\n"; //$NON-NLS-1$
            }
            reply.setText(text);
        } else {
            format.applyPattern(messages.getString("information")); //$NON-NLS-1$
            String rep = format.format(new Object[]{
                name
            }) + "\n"; //$NON-NLS-1$
            format.applyPattern(messages.getString("msg_count")); //$NON-NLS-1$
            rep += format.format(new Object[]{
                SqlUtil.getInt("stats", id, "msg_count").orElse(0) //$NON-NLS-1$ //$NON-NLS-2$
            }) + "\n"; //$NON-NLS-1$
            format.applyPattern(messages.getString("karma")); //$NON-NLS-1$
            rep += format.format(new Object[]{
                SqlUtil.getInt("stats", id, "pat_count").orElse(0) //$NON-NLS-1$ //$NON-NLS-2$
            });
            reply.setText(rep);
        }
        bot.execute(reply);
    }
}

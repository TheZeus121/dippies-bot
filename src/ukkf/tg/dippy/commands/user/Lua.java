package ukkf.tg.dippy.commands.user;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.regex.Matcher;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LoadState;
import org.luaj.vm2.LuaString;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaThread;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.compiler.LuaC;
import org.luaj.vm2.lib.Bit32Lib;
import org.luaj.vm2.lib.DebugLib;
import org.luaj.vm2.lib.PackageLib;
import org.luaj.vm2.lib.StringLib;
import org.luaj.vm2.lib.TableLib;
import org.luaj.vm2.lib.ZeroArgFunction;
import org.luaj.vm2.lib.jse.JseBaseLib;
import org.luaj.vm2.lib.jse.JseMathLib;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.MustSign;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;

@Command
@Regex("^/lua\\s.*$")
@Text
@CanSkip
@MustSign
// @GlobalAdmin
public class Lua extends Action {
    
    private static Globals server_globals;
    
    // Simple read-only table whose contents are initialized from another table.
    static class ReadOnlyLuaTable extends LuaTable {
        public ReadOnlyLuaTable(LuaValue table) {
            presize(table.length(), 0);
            for (Varargs n = table.next(LuaValue.NIL); !n.arg1().isnil(); n = table.next(n.arg1())) {
                LuaValue key = n.arg1();
                LuaValue value = n.arg(2);
                super.rawset(key, value.istable() ? new ReadOnlyLuaTable(value) : value);
            }
        }
        
        @Override
        public LuaValue setmetatable(LuaValue metatable) {
            return error("table is read-only"); //$NON-NLS-1$
        }
        
        @Override
        public void set(int key, LuaValue value) {
            error("table is read-only"); //$NON-NLS-1$
        }
        
        @Override
        public void rawset(int key, LuaValue value) {
            error("table is read-only"); //$NON-NLS-1$
        }
        
        @Override
        public void rawset(LuaValue key, LuaValue value) {
            error("table is read-only"); //$NON-NLS-1$
        }
        
        @Override
        public LuaValue remove(int pos) {
            return error("table is read-only"); //$NON-NLS-1$
        }
    }
    
    @Override
    public void init() {
        super.init();
        
        // server_globals will be used to load, compile user scripts
        server_globals = new Globals();
        server_globals.load(new JseBaseLib());
        server_globals.load(new PackageLib());
        server_globals.load(new StringLib());
        server_globals.load(new JseMathLib());
        
        LoadState.install(server_globals);
        LuaC.install(server_globals);
        
        LuaString.s_metatable = new ReadOnlyLuaTable(LuaString.s_metatable);
    }
    
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        String script = msg.getText().split("\\s", 2)[1]; //$NON-NLS-1$
        
        Globals user_globals = new Globals();
        user_globals.load(new JseBaseLib());
        user_globals.load(new PackageLib());
        user_globals.load(new StringLib());
        user_globals.load(new JseMathLib());
        user_globals.load(new Bit32Lib());
        user_globals.load(new TableLib());
        
        // LuajavaLib lets access Java VM so that's out
        // CoroutineLib lets make threads that aren't controlled by server so that's out
        // JseIoLib and JseOsLib, should make something like io.write accessible TODO
        // ppl can run and compile other scripts
        LoadState.install(user_globals);
        LuaC.install(user_globals);
        
        // gonna load the debug library because it lets us create hook functions for
        // limiting script to cycle count
        // but we don't want to expose it to the user
        user_globals.load(new DebugLib());
        LuaValue sethook = user_globals.get("debug").get("sethook"); //$NON-NLS-1$ //$NON-NLS-2$
        user_globals.set("debug", LuaValue.NIL); //$NON-NLS-1$
        
        LuaValue chunk;
        try {
            chunk = server_globals.load(script, "main", user_globals); //$NON-NLS-1$
        } catch (Exception e) {
            SendMessage reply = new SendMessage();
            reply.setChatId(msg.getChatId());
            reply.setReplyToMessageId(msg.getMessageId());
            reply.setText(messages.getString("parse_error") + "\n" + e.getMessage()); //$NON-NLS-1$ //$NON-NLS-2$
            
            bot.execute(reply);
            return;
        }
        
        LuaThread thread = new LuaThread(user_globals, chunk);
        LuaValue hookfunc = new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                // gonna throw a Java(!) Error when too many instructions have been reached
                throw new Error(messages.getString("script_too_long")); //$NON-NLS-1$
            }
        };
        
        final int max_instr = 1000;
        sethook.invoke(LuaValue.varargsOf(new LuaValue[]{
            thread, hookfunc, LuaValue.EMPTYSTRING, LuaValue.valueOf(max_instr)
        }));
        
        String ret = ""; //$NON-NLS-1$
        
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        user_globals.STDOUT = new PrintStream(out);
        user_globals.STDERR = new PrintStream(out);
        
        try {
            Varargs result = thread.resume(LuaValue.NIL);
            ret = "\n\n-> " + result.toString(); //$NON-NLS-1$
        } catch (Exception e) {
            ret = "\n\n!> " + e.getMessage(); //$NON-NLS-1$
        }
        
        ret = out.toString("utf-8") + ret; //$NON-NLS-1$
        
        SendMessage reply = new SendMessage();
        reply.setChatId(msg.getChatId());
        reply.setReplyToMessageId(msg.getMessageId());
        reply.setText(ret);
        
        bot.execute(reply);
    }
    
}

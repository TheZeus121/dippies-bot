package ukkf.tg.dippy.commands.user;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import ukkf.tg.BotConfig;
import ukkf.tg.api.Action;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.MustSign;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;
import ukkf.tg.util.NameUtil;
import ukkf.tg.util.SqlUtil;

@Command
@Regex("/pat")
@Text
@MustSign
@InlineCommand
public class Pat extends Action {
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        User patter = msg.getFrom();
        SendMessage reply = new SendMessage();
        reply.setChatId(msg.getChatId().toString());
        User patted = null;
        if (msg.getReplyToMessage() != null) {
            patted = msg.getReplyToMessage().getFrom();
        }
        if (msg.getReplyToMessage() == null) {
            reply.setReplyToMessageId(msg.getMessageId());
            InlineKeyboardMarkup mark = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> yep = new ArrayList<>();
            ArrayList<InlineKeyboardButton> row1 = new ArrayList<>();
            yep.add(row1);
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(messages.getString("get_help")); //$NON-NLS-1$
            button.setUrl("http://telegram.me/dippies_bot?start=XDhelp-pat"); //$NON-NLS-1$
            row1.add(button);
            mark.setKeyboard(yep);
            reply.setReplyMarkup(mark);
            reply.setText(messages.getString("pat_reply")); //$NON-NLS-1$
            reply.enableMarkdown(true);
            reply.disableWebPagePreview();
        } else if (patted == null) {
            reply.setReplyToMessageId(msg.getMessageId());
            reply.setText(messages.getString("something_wrong")); //$NON-NLS-1$
        } else if (patted.getId().equals(BotConfig.BOT_ID)) {
            reply.setReplyToMessageId(msg.getMessageId());
            reply.setText(messages.getString("pat_bot")); //$NON-NLS-1$
        } else if (!patter.getId().equals(patted.getId())) {
            int pats;
            int id = patted.getId();
            pats = SqlUtil.getInt("stats", id, "pat_count").orElse(0); //$NON-NLS-1$ //$NON-NLS-2$
            if (pats > 999)
                pats = 999;
            pats++;
            SqlUtil.setInt("stats", id, "pat_count", pats); //$NON-NLS-1$ //$NON-NLS-2$
            reply.setReplyToMessageId(msg.getReplyToMessage().getMessageId());
            format.applyPattern(messages.getString("patted")); //$NON-NLS-1$
            reply.setText(format.format(new Object[]{
                NameUtil.getName(patter)
            }));
        } else {
            reply.setReplyToMessageId(msg.getMessageId());
            reply.setText(messages.getString("pat_self")); //$NON-NLS-1$
        }
        bot.execute(reply);
    }
}

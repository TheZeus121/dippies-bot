package ukkf.tg.dippy.commands.user;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import ukkf.tg.BotConfig;
import ukkf.tg.api.Action;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.MustSign;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;
import ukkf.tg.util.NameUtil;
import ukkf.tg.util.SqlUtil;

@Command
@Regex("/slap")
@Text
@InlineCommand
@MustSign
public class Slap extends Action {
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        User patter = msg.getFrom();
        SendMessage reply = new SendMessage();
        reply.setChatId(msg.getChatId().toString());
        User patted = null;
        if (msg.isReply()) {
            if (msg.getReplyToMessage().getForwardDate() != null)
                patted = msg.getReplyToMessage().getForwardFrom();
            else
                patted = msg.getReplyToMessage().getFrom();
        }
        if (!msg.isReply()) {
            reply.setReplyToMessageId(msg.getMessageId());
            InlineKeyboardMarkup mark = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> yep = new ArrayList<>();
            ArrayList<InlineKeyboardButton> row1 = new ArrayList<>();
            yep.add(row1);
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(messages.getString("get_help")); //$NON-NLS-1$
            button.setUrl("http://telegram.me/dippies_bot?start=XDhelp-slap"); //$NON-NLS-1$
            row1.add(button);
            mark.setKeyboard(yep);
            reply.setReplyMarkup(mark);
            reply.setText(messages.getString("slap_reply")); //$NON-NLS-1$
            reply.enableMarkdown(true);
            reply.disableWebPagePreview();
        } else if (patted == null) {
            reply.setReplyToMessageId(msg.getMessageId());
            reply.setText(messages.getString("something_wrong")); //$NON-NLS-1$
        } else if (patted.getId().equals(BotConfig.BOT_ID)) {
            reply.setReplyToMessageId(msg.getMessageId());
            reply.setText(messages.getString("slap_bot")); //$NON-NLS-1$
        } else if (!patter.getId().equals(patted.getId())) {
            int pats;
            int id = patted.getId();
            pats = SqlUtil.getInt("stats", id, "pat_count").orElse(0); //$NON-NLS-1$ //$NON-NLS-2$
            if (pats <= 0) {
                int tmp = pats;
                pats = SqlUtil.getInt("stats", patter.getId(), "pat_count").orElse(0); //$NON-NLS-1$ //$NON-NLS-2$
                pats += tmp - 1;
                if (pats < -1000)
                    pats = -1000;
                SqlUtil.setInt("stats", patter.getId(), "pat_count", pats); //$NON-NLS-1$ //$NON-NLS-2$
            } else {
                pats--;
                if (pats < -1000)
                    pats = -1000;
                SqlUtil.setInt("stats", id, "pat_count", pats); //$NON-NLS-1$ //$NON-NLS-2$
            }
            reply.setReplyToMessageId(msg.getReplyToMessage().getMessageId());
            format.applyPattern(messages.getString("slapped")); //$NON-NLS-1$
            reply.setText(format.format(new Object[]{
                NameUtil.getName(patter)
            }));
        } else {
            reply.setReplyToMessageId(msg.getMessageId());
            reply.setText(messages.getString("slap_self")); //$NON-NLS-1$
        }
        bot.execute(reply);
    }
}

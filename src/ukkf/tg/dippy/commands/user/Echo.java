package ukkf.tg.dippy.commands.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendAudio;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.send.SendSticker;
import org.telegram.telegrambots.meta.api.methods.send.SendVideo;
import org.telegram.telegrambots.meta.api.methods.send.SendVideoNote;
import org.telegram.telegrambots.meta.api.methods.send.SendVoice;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;

@Command
@Regex("^/echo")
@Text
@CanSkip
public class Echo extends Action {
    
    private Pattern file_id;
    
    private String parse(String text, Message msg, TelegramLongPollingBot bot) throws TelegramApiException {
        text = text.trim();
        
        text = text.replaceAll(Pattern.quote("&"), Matcher.quoteReplacement("&amp;")); //$NON-NLS-1$ //$NON-NLS-2$
        text = text.replaceAll(Pattern.quote("<"), Matcher.quoteReplacement("&lt;")); //$NON-NLS-1$ //$NON-NLS-2$
        text = text.replaceAll(Pattern.quote(">"), Matcher.quoteReplacement("&gt;")); //$NON-NLS-1$ //$NON-NLS-2$
        text = text.replaceAll(Pattern.quote("\""), Matcher.quoteReplacement("&quot;")); //$NON-NLS-1$ //$NON-NLS-2$
        
        char[] arr = text.toCharArray();
        StringBuilder sb = new StringBuilder();
        
        // find bold and italics
        boolean bolded = false;
        boolean italicized = false;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == '\\') {
                if (i == arr.length - 1) {
                    sb.append('\\');
                } else {
                    i++;
                    sb.append("&#x"); //$NON-NLS-1$
                    sb.append(Integer.toHexString(arr[i]));
                    sb.append(';');
                }
            } else if (arr[i] == '*') {
                bolded = !bolded;
                if (bolded)
                    sb.append("<b>"); //$NON-NLS-1$
                else
                    sb.append("</b>"); //$NON-NLS-1$
            } else if (arr[i] == '_') {
                italicized = !italicized;
                if (italicized)
                    sb.append("<i>"); //$NON-NLS-1$
                else
                    sb.append("</i>"); //$NON-NLS-1$
            } else {
                sb.append(arr[i]);
            }
        }
        
        text = sb.toString();
        arr = text.toCharArray();
        sb.setLength(0);
        
        boolean striking = false;
        boolean skipping = false;
        for (int i = 0; i < arr.length; i++) {
            if (skipping) {
                if (arr[i] == ';')
                    skipping = false;
            } else if (striking) {
                if (arr[i] == '&') {
                    skipping = true;
                } else if (arr[i] == '~') {
                    striking = false;
                } else {
                    sb.append('\u0336');
                    sb.append(arr[i]);
                }
            } else if (arr[i] == '~') {
                striking = true;
                if (i != arr.length - 1) {
                    i++;
                }
                sb.append(arr[i]);
            } else {
                sb.append(arr[i]);
            }
        }
        
        HashMap<String, String> references = new HashMap<>();
        ArrayList<Integer> reflocs = new ArrayList<>();
        HashMap<Integer, String> locToRef = new HashMap<>();
        
        text = sb.toString();
        arr = text.toCharArray();
        sb.setLength(0);
        StringBuilder title = new StringBuilder();
        StringBuilder ref = new StringBuilder();
        boolean readingTitle = false;
        boolean readingHref = false;
        boolean readingRef = false;
        boolean readingDef = false;
        // find links
        for (int i = 0; i < arr.length; i++) {
            if (readingTitle) {
                if (arr[i] == ']') {
                    readingTitle = false;
                    if (i != arr.length - 1) {
                        i++;
                        if (arr[i] == '(') {
                            readingHref = true;
                            continue;
                        } else if (arr[i] == '[') {
                            readingRef = true;
                            continue;
                        } else if (arr[i] == ':') {
                            readingDef = true;
                            continue;
                        }
                    }
                    String reference = title.toString().trim().toLowerCase();
                    title.setLength(0);
                    sb.append("<a href=\""); //$NON-NLS-1$
                    if (references.containsKey(reference)) {
                        sb.append(references.get(reference));
                    } else {
                        reflocs.add(sb.length());
                        locToRef.put(sb.length(), reference);
                    }
                    sb.append("\">"); //$NON-NLS-1$
                    sb.append(reference);
                    sb.append("</a>"); //$NON-NLS-1$
                } else {
                    title.append(arr[i]);
                }
            } else if (readingHref) {
                if (arr[i] == ')') {
                    readingHref = false;
                    String name = title.toString().trim();
                    String link = ref.toString().trim();
                    title.setLength(0);
                    ref.setLength(0);
                    sb.append("<a href=\""); //$NON-NLS-1$
                    sb.append(link);
                    sb.append("\">"); //$NON-NLS-1$
                    sb.append(name);
                    sb.append("</a>"); //$NON-NLS-1$
                } else {
                    ref.append(arr[i]);
                }
            } else if (readingRef) {
                if (arr[i] == ']') {
                    readingRef = false;
                    String name = title.toString().trim();
                    String reference = ref.toString().trim().toLowerCase();
                    title.setLength(0);
                    ref.setLength(0);
                    sb.append("<a href=\""); //$NON-NLS-1$
                    if (references.containsKey(reference)) {
                        sb.append(references.get(reference));
                    } else {
                        reflocs.add(sb.length());
                        locToRef.put(sb.length(), reference);
                    }
                    sb.append("\">"); //$NON-NLS-1$
                    sb.append(name);
                    sb.append("</a>"); //$NON-NLS-1$
                } else {
                    ref.append(arr[i]);
                }
            } else if (readingDef) {
                if (arr[i] == '\n') {
                    readingDef = false;
                    String name = title.toString().trim().toLowerCase();
                    String reference = ref.toString().trim();
                    title.setLength(0);
                    ref.setLength(0);
                    references.put(name, reference);
                } else {
                    ref.append(arr[i]);
                }
            } else if (arr[i] == '[') {
                readingTitle = true;
            } else {
                sb.append(arr[i]);
            }
        }
        
        if (readingTitle) {
            sb.append('[');
            sb.append(title.toString());
        } else if (readingHref) {
            sb.append('[');
            sb.append(title.toString());
            sb.append("]("); //$NON-NLS-1$
            sb.append(ref.toString());
        } else if (readingRef) {
            sb.append('[');
            sb.append(title.toString());
            sb.append("]["); //$NON-NLS-1$
            sb.append(ref.toString());
        } else if (readingDef) {
            String name = title.toString().trim().toLowerCase();
            String reference = ref.toString().trim();
            references.put(name, reference);
        }
        
        text = sb.toString();
        sb.setLength(0);
        int end = 0;
        for (int loc : reflocs) {
            String reference = locToRef.get(loc);
            if (references.containsKey(reference)) {
                sb.append(text.substring(end, loc));
                sb.append(references.get(reference));
                end = loc;
            }
        }
        sb.append(text.substring(end));
        
        text = sb.toString();
        
        Matcher m = file_id.matcher(text);
        while (m.find()) {
            String type = m.group(1);
            String id = m.group(2);
            if (type != null && type.equals("image")) { //$NON-NLS-1$
                SendPhoto pic = new SendPhoto();
                pic.setChatId(msg.getChatId());
                pic.setReplyToMessageId(msg.getMessageId());
                pic.setPhoto(id);
                bot.execute(pic);
            } else if (type != null && type.equals("video")) { //$NON-NLS-1$
                SendVideo vid = new SendVideo();
                vid.setChatId(msg.getChatId());
                vid.setReplyToMessageId(msg.getMessageId());
                vid.setVideo(id);
                bot.execute(vid);
            } else if (type != null && type.equals("audio")) { //$NON-NLS-1$
                SendAudio mus = new SendAudio();
                mus.setChatId(msg.getChatId());
                mus.setReplyToMessageId(msg.getMessageId());
                mus.setAudio(id);
                bot.execute(mus);
            } else if (type != null && type.equals("voice")) { //$NON-NLS-1$
                SendVoice voice = new SendVoice();
                voice.setChatId(msg.getChatId());
                voice.setReplyToMessageId(msg.getMessageId());
                voice.setVoice(id);
                bot.execute(voice);
            } else if (type != null && type.equals("sticker")) { //$NON-NLS-1$
                SendSticker stick = new SendSticker();
                stick.setChatId(msg.getChatId());
                stick.setReplyToMessageId(msg.getMessageId());
                stick.setSticker(id);
                bot.execute(stick);
            } else if (type != null && type.equals("video_note")) { //$NON-NLS-1$
                SendVideoNote vid = new SendVideoNote();
                vid.setChatId(msg.getChatId());
                vid.setReplyToMessageId(msg.getMessageId());
                vid.setVideoNote(id);
                bot.execute(vid);
            } else {// "gif", "file", "", null
                SendDocument doc = new SendDocument();
                doc.setChatId(msg.getChatId());
                doc.setReplyToMessageId(msg.getMessageId());
                doc.setDocument(id);
                bot.execute(doc);
            }
        }
        text = text.replaceAll(file_id.pattern(), ""); //$NON-NLS-1$
        
        return text.trim();
    }
    
    @Override
    public void init() {
        super.init();
        file_id = Pattern.compile("###file_id!(.*)###:(\\S*)"); //$NON-NLS-1$
    }
    
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        String[] tokens = msg.getText().split(" ", 2); //$NON-NLS-1$
        if (!msg.isReply() && tokens.length < 2) {
            SendMessage reply = new SendMessage();
            InlineKeyboardMarkup mark = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> yep = new ArrayList<>();
            ArrayList<InlineKeyboardButton> row1 = new ArrayList<>();
            yep.add(row1);
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(messages.getString("get_help")); //$NON-NLS-1$
            button.setUrl("http://telegram.me/dippies_bot?start=XDhelp-echo"); //$NON-NLS-1$
            row1.add(button);
            mark.setKeyboard(yep);
            reply.setReplyMarkup(mark);
            reply.setText(messages.getString("wrong_syntax")); //$NON-NLS-1$
            reply.setChatId(msg.getChatId().toString());
            reply.setReplyToMessageId(msg.getMessageId());
            reply.enableMarkdown(true);
            reply.disableWebPagePreview();
            bot.execute(reply);
            return;
        }
        if (msg.isReply()) {
            msg = msg.getReplyToMessage();
            tokens = new String[2];
            tokens[1] = msg.getText();
        }
        String text = parse(tokens[1], msg, bot);
        if (text == null || text.length() == 0)
            return;
        SendMessage reply = new SendMessage();
        reply.setChatId(msg.getChatId().toString());
        reply.setReplyToMessageId(msg.getMessageId());
        reply.setText(text);
        reply.enableHtml(true);
        try {
            bot.execute(reply);
        } catch (Exception e) {
            reply.setText(messages.getString("user_error")); //$NON-NLS-1$
            bot.execute(reply);
        }
    }
}

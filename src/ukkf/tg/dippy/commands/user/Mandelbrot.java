package ukkf.tg.dippy.commands.user;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.regex.Matcher;

import javax.imageio.ImageIO;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.Image;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.util.ApiUtil;

@Command
@Regex("/mandelbrot")
@InlineCommand
@Image
public class Mandelbrot extends Action {
    public static final int SIZE = 500;
    public static final double BAILOUT = 10000;
    public static final int MAX_ITER = 1000;
    
    private static final Color[] basePalette = {
        Color.YELLOW,
        Color.MAGENTA,
        Color.CYAN,
        Color.GREEN,
        Color.RED
    };
    
    private double d(double x, double y) {
        return x * x + y * y;
    }
    
    private float clamp(float a, float min, float max) {
        if (min > a)
            return min;
        if (max < a)
            return max;
        return a;
    }
    
    private Color interpolate(double i, Color[] palette) {
        Color c1 = palette[((int) i) % (palette.length)];
        Color c2 = palette[((int) i + 1) % (palette.length)];
        float x = ((float) i) - ((int) i);
        return new Color(clamp((c2.getRed() * x + c1.getRed() * (1 - x)) / 255, 0, 1),
                clamp((c2.getBlue() * x + c1.getBlue() * (1 - x)) / 255, 0, 1),
                clamp((c2.getGreen() * x + c1.getGreen() * (1 - x)) / 255, 0, 1));
    }
    
    private Color getColorFromPalette(double loc, int offset) {
        return interpolate(loc * basePalette.length + offset, basePalette);
    }
    
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        new Thread(new Runnable() {
            @Override
            public void run() {
                BufferedImage image = null;
                double a0 = 0;
                double b0 = 0;
                double a1 = 0;
                double b1 = 0;
                try {
                    boolean noImage = true;
                    while (noImage) {
                        int offset = rand.nextInt(basePalette.length);
                        image = new BufferedImage(SIZE, SIZE, BufferedImage.TYPE_INT_RGB);
                        Graphics g = image.getGraphics();
                        double[][] map = new double[SIZE + 1][SIZE + 1];
                        double[] histogram1 = new double[MAX_ITER + 1];
                        for (int i = 0; i < MAX_ITER + 1; i++)
                            histogram1[i] = 0;
                        int total_count = 0;
                        double[] histogram2 = new double[MAX_ITER + 1];
                        Color[] palette = new Color[MAX_ITER + 1];
                        a0 = rand.nextDouble() * 4 - 2;
                        b0 = rand.nextDouble() * 3 - 1.5;
                        double _1 = rand.nextDouble() * 3;
                        a1 = a0 + _1;
                        b1 = b0 + _1;
                        final double step = (a1 - a0) / SIZE;
                        for (int ai = 0; ai <= SIZE; ai++) {
                            for (int bi = 0; bi <= SIZE; bi++) {
                                double a = ai * step + a0;
                                double b = bi * step + b0;
                                double x = a;
                                double y = b;
                                double i;
                                for (i = 0; d(x, y) < BAILOUT * BAILOUT && i < MAX_ITER; i++) {
                                    double tmp = x * x - y * y + a;
                                    y = 2 * x * y + b;
                                    x = tmp;
                                }
                                
                                if (d(x, y) >= 2 * 2) {
                                    double log_zn = Math.log(d(x, y)) / 2;
                                    double nu = Math.log(log_zn / Math.log(2)) / Math.log(2);
                                    i += 1 - nu;
                                    map[ai][bi] = i;
                                    histogram1[((int) i) % (MAX_ITER + 1)] += ((double) i) - ((int) i);
                                    histogram1[((int) i + 1) % (MAX_ITER + 1)] += ((int) i + 1) - ((double) i);
                                    total_count++;
                                } else {
                                    map[ai][bi] = -1;
                                }
                            }
                        }
                        
                        for (int i = 0; i < MAX_ITER + 1; i++) {
                            histogram2[i] = histogram1[i] / (double) total_count;
                            if (i != 0)
                                histogram2[i] += histogram2[i - 1];
                            palette[i] = getColorFromPalette(histogram2[i], offset);
                        }
                        
                        for (int x = 0; x <= SIZE; x++) {
                            for (int y = 0; y <= SIZE; y++) {
                                if (map[x][y] == -1) {
                                    noImage = false;
                                    g.setColor(Color.BLACK);
                                } else
                                    g.setColor(interpolate(map[x][y], palette));
                                g.drawRect(x, y, 1, 1);
                            }
                        }
                    }
                    File output = File.createTempFile("dip", "png"); //$NON-NLS-1$ //$NON-NLS-2$
                    ImageIO.write(image, "png", output); //$NON-NLS-1$
                    SendPhoto photo = new SendPhoto();
                    photo.setPhoto(output);
                    photo.setChatId(msg.getChatId());
                    photo.setReplyToMessageId(msg.getMessageId());
                    photo.setCaption("x: from " + a0 + " to " + a1 + "\ny: from " + b0 + " to " + b1);
                    bot.execute(photo);
                } catch (Exception e) {
                    try {
                        SendMessage reply = new SendMessage();
                        reply.setChatId(msg.getChatId());
                        reply.setReplyToMessageId(msg.getMessageId());
                        reply.setText("Some sort of an error occured!");
                        bot.execute(reply);
                    } catch (Exception e1) {
                        ApiUtil.notifyException(bot, "", e1); //$NON-NLS-1$
                    }
                    ApiUtil.notifyException(bot, "", e); //$NON-NLS-1$
                }
            }
        }).start();
    }
}

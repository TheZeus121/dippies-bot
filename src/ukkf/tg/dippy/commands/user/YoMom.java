package ukkf.tg.dippy.commands.user;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.NotGroups;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;

@Command
@Regex("/yomama")
@Text
@InlineCommand
@NotGroups
@CanSkip
public class YoMom extends Action {
    private ArrayList<String> db;
    
    @Override
    public void init() {
        super.init();
        this.db = new ArrayList<String>();
        try {
            Scanner s = new Scanner(new FileInputStream("yomama")); //$NON-NLS-1$
            while (s.hasNextLine()) {
                this.db.add(s.nextLine().trim());
            }
            s.close();
        } catch (FileNotFoundException e) {
            System.err.println("Error while loading your mom. Shutting down"); //$NON-NLS-1$
            System.err.println(e.getMessage());
            System.exit(1);
        }
    }
    
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        SendMessage reply = new SendMessage();
        reply.setText(this.db.get(rand.nextInt(this.db.size())));
        reply.setChatId(msg.getChatId().toString());
        reply.setReplyToMessageId(msg.getMessageId());
        bot.execute(reply);
    }
}

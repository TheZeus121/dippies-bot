package ukkf.tg.dippy.commands.user.flickr;

import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.Flickr;
import ukkf.tg.api.annot.Image;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.Regex;

@Command
@Regex("/sushi")
@Flickr("sushi")
@InlineCommand
@Image
@CanSkip
public class Sushi extends Action {
	@Override
	public void run(Update upd, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot) throws Exception {
		SendPhoto img = new SendPhoto();
		img.setPhoto(images.get(rand.nextInt(images.getPerPage())).getUrl());
		if (msg.isReply())
			img.setReplyToMessageId(msg.getReplyToMessage().getMessageId());
		else
			img.setReplyToMessageId(msg.getMessageId());
		img.setCaption(messages.getString("sushi")); //$NON-NLS-1$
		img.setChatId(msg.getChatId().toString());
		bot.execute(img);
	}

}

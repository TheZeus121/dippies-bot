package ukkf.tg.dippy.commands.user.flickr;

import java.util.regex.Matcher;

import com.flickr4java.flickr.photos.Photo;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.Flickr;
import ukkf.tg.api.annot.Image;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.Regex;

@Command
@Regex("/t+r+ee+")
@Flickr({ "forest", "trees", "woods" })
@Image
@InlineCommand
@CanSkip
public class Trees extends Action {
	@Override
	public void run(Update upd, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot) throws Exception {
		Photo photo = images.get(rand.nextInt(images.getPerPage()));
		SendPhoto reply = new SendPhoto();
		reply.setCaption("/TREEEEEES :D"); //$NON-NLS-1$
		reply.setChatId(msg.getChatId());
		reply.setReplyToMessageId(msg.getMessageId());
		reply.setPhoto(photo.getUrl());
		bot.execute(reply);
	}
}

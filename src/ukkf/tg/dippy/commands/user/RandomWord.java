package ukkf.tg.dippy.commands.user;

import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.regex.Matcher;

import org.apache.commons.io.FileUtils;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.BotConfig;
import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;

@Command
@Regex("/random")
@InlineCommand
@Text
@CanSkip
public class RandomWord extends Action {
    private class WordnikRandomJSON {
        public String word;
    }
    
    private void somethingWrong(Message msg, TelegramLongPollingBot bot) throws Exception {
        SendMessage reply = new SendMessage();
        reply.setText(messages.getString("random_error")); //$NON-NLS-1$
        reply.setChatId(msg.getChatId());
        reply.setReplyToMessageId(msg.getMessageId());
        bot.execute(reply);
    }
    
    @Override
    public void run(Update upd, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot) throws Exception {
        URL url = new URL("http://api.wordnik.com:80/v4/words.json/randomWord?api_key=" + BotConfig.WORDNIK_APIKEY); //$NON-NLS-1$
        
        File tmp = File.createTempFile("dip", "json"); //$NON-NLS-1$//$NON-NLS-2$
        
        try {
            FileUtils.copyURLToFile(url, tmp);
        } catch (Exception e) {
            somethingWrong(msg, bot);
            return;
        }
        
        WordnikRandomJSON json = new WordnikRandomJSON();
        json = gson.fromJson(new FileReader(tmp), WordnikRandomJSON.class);
        
        StringBuilder text = new StringBuilder();
        text.append(messages.getString("random")); //$NON-NLS-1$
        text.append("\n\n["); //$NON-NLS-1$
        text.append(json.word);
        text.append("](http://www.wordnik.com/words/"); //$NON-NLS-1$
        text.append(json.word);
        text.append(")\n\n_Powered by Wordnik_"); //$NON-NLS-1$
        
        SendMessage reply = new SendMessage();
        reply.setChatId(Long.toString(msg.getChatId()));
        reply.setReplyToMessageId(msg.getMessageId());
        reply.enableMarkdown(true);
        reply.setText(text.toString());
        bot.execute(reply);
    }
    
}

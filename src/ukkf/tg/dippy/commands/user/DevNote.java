package ukkf.tg.dippy.commands.user;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.ForwardMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import ukkf.tg.BotConfig;
import ukkf.tg.api.Action;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.NotChannels;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;

@Command
@Regex("^/devnote")
@Text
@NotChannels
public class DevNote extends Action {
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        String[] tokens = msg.getText().split(" "); //$NON-NLS-1$
        if (tokens.length < 2) {
            SendMessage reply = new SendMessage();
            InlineKeyboardMarkup mark = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> yep = new ArrayList<>();
            ArrayList<InlineKeyboardButton> row1 = new ArrayList<>();
            yep.add(row1);
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(messages.getString("get_help")); //$NON-NLS-1$
            button.setUrl("http://telegram.me/dippies_bot?start=XDhelp-devnote"); //$NON-NLS-1$
            row1.add(button);
            mark.setKeyboard(yep);
            reply.setReplyMarkup(mark);
            reply.setText(messages.getString("wrong_syntax")); //$NON-NLS-1$
            reply.setChatId(msg.getChatId().toString());
            reply.setReplyToMessageId(msg.getMessageId());
            reply.enableMarkdown(true);
            reply.disableWebPagePreview();
            bot.execute(reply);
            return;
        }
        ForwardMessage note_msg = new ForwardMessage();
        note_msg.setMessageId(msg.getMessageId());
        note_msg.setChatId(Integer.toString(BotConfig.BOT_OWNER));
        note_msg.setFromChatId(msg.getChatId().toString());
        bot.execute(note_msg);
        SendMessage reply = new SendMessage();
        reply.setChatId(msg.getChatId().toString());
        reply.setReplyToMessageId(msg.getMessageId());
        reply.setText(messages.getString("note_sent")); //$NON-NLS-1$
        bot.execute(reply);
    }
}

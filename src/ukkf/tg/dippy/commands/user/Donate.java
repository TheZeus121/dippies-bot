package ukkf.tg.dippy.commands.user;

import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.BotConfig;
import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;

@Command
@Regex("/donate")
@InlineCommand
@Text
@CanSkip
public class Donate extends Action {
    @Override
    public void run(Update upd, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot) throws Exception {
        SendMessage reply = new SendMessage();
        reply.setChatId(msg.getChatId());
        reply.setReplyToMessageId(msg.getMessageId());
        format.applyPattern(messages.getString("donate_paypal")); //$NON-NLS-1$
        reply.setText(messages.getString("donate1") + "\n\n" //$NON-NLS-1$ //$NON-NLS-2$
                + BotConfig.DONATION_ADDRESS + "\n\n" + messages.getString("donate2") + //$NON-NLS-1$ //$NON-NLS-2$
                "\n" + format.format(new Object[]{ //$NON-NLS-1$
                    "paypal.me/TheZeus121" //$NON-NLS-1$
                }) + "\n:)"); //$NON-NLS-1$
        reply.disableWebPagePreview();
        bot.execute(reply);
    }
    
}

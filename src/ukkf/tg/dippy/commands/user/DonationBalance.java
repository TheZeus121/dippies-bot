package ukkf.tg.dippy.commands.user;

import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;
import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.BotConfig;
import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;
import ukkf.tg.util.ApiUtil;

@Command
@Regex("/balance")
@Text
@InlineCommand
@CanSkip
public class DonationBalance extends Action {
    private class Data {
        public String confirmed_received_value = messages.getString("something_wrong"); //$NON-NLS-1$
    }
    
    private class Received {
        public String status = "fail"; //$NON-NLS-1$
        public Data data = null;
    }
    
    @Override
    public void run(Update upd, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot) throws Exception {
        URL url;
        url = new URL("https://chain.so/api/v2/get_address_received/DOGE/" + BotConfig.DONATION_ADDRESS); //$NON-NLS-1$
        
        URLConnection conn;
        conn = url.openConnection();
        conn.setRequestProperty("User-Agent", "Dippy/1.0 (Heroku; Java; en-US)"); //$NON-NLS-1$ //$NON-NLS-2$
        
        Scanner s;
        s = new Scanner(conn.getInputStream());
        
        StringBuilder builder = new StringBuilder();
        while (s.hasNextLine()) {
            builder.append(s.nextLine());
        }
        Received obj = gson.fromJson(builder.toString(), Received.class);
        s.close();
        SendMessage reply = new SendMessage();
        reply.setChatId(msg.getChatId());
        reply.setReplyToMessageId(msg.getMessageId());
        if ("success".equals(obj.status)) { //$NON-NLS-1$
            Object[] args = {
                obj.data.confirmed_received_value
            };
            format.applyPattern(messages.getString("donate3")); //$NON-NLS-1$
            reply.setText(format.format(args));
        } else if ("fail".equals(obj.status)) { //$NON-NLS-1$
            reply.setText(messages.getString("donate4")); //$NON-NLS-1$
            ApiUtil.notifyMe("Error /balance\n" + builder.toString(), bot); //$NON-NLS-1$
        } else {
            reply.setText(messages.getString("donate5")); //$NON-NLS-1$
            ApiUtil.notifyMe("OMG OMG OMG SHIT CONNECTION? \n" + builder.toString(), bot); //$NON-NLS-1$
        }
        
        bot.execute(reply);
    }
}

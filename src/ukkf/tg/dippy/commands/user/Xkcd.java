package ukkf.tg.dippy.commands.user;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import org.apache.commons.io.FileUtils;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.Image;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.Regex;

@Command
@Regex("/xkcd")
@InlineCommand
@Image
@CanSkip
public class Xkcd extends Action {
    private class XkcdJson {
        public String title;
        public String img;
        // public Integer num;
        
        private XkcdJson() {}
    }
    
    private void noPic(TelegramLongPollingBot bot, Message msg) throws Exception {
        SendMessage reply = new SendMessage();
        reply.setText(messages.getString("no_image")); //$NON-NLS-1$
        reply.setChatId(msg.getChatId().toString());
        reply.setReplyToMessageId(msg.getMessageId());
        bot.execute(reply);
    }
    
    private Pattern xkcd;
    
    @Override
    public void init() {
        super.init();
        xkcd = Pattern.compile("/xkcd(?:@.*)? (\\d+)", Pattern.CASE_INSENSITIVE); //$NON-NLS-1$
    }
    
    @Override
    public void run(Update update, Message msg, int which, Matcher matcherarg, TelegramLongPollingBot bot)
            throws Exception {
        Matcher matcher = xkcd.matcher(msg.getText());
        URL url = null;
        if (matcher.find()) {
            String num = matcher.group(1);
            url = new URL("https://xkcd.com/" + num + "/info.0.json"); //$NON-NLS-1$//$NON-NLS-2$
        } else {
            url = new URL("https://xkcd.com/info.0.json"); //$NON-NLS-1$
        }
        
        File tmp;
        tmp = File.createTempFile("dip", "json"); //$NON-NLS-1$ //$NON-NLS-2$
        
        try {
            FileUtils.copyURLToFile(url, tmp);
        } catch (IOException e1) {
            noPic(bot, msg);
            return;
        }
        
        XkcdJson json = new XkcdJson();
        try {
            json = gson.fromJson(new FileReader(tmp), XkcdJson.class);
        } catch (JsonSyntaxException | JsonIOException | FileNotFoundException e1) {
            // IGNORE
        }
        SendPhoto reply = new SendPhoto();
        reply.setChatId(msg.getChatId().toString());
        reply.setCaption(json.title);
        reply.setReplyToMessageId(msg.getMessageId());
        String surl = json.img;
        reply.setPhoto(surl);
        bot.execute(reply);
    }
    
}

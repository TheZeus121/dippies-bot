package ukkf.tg.dippy.commands.user;

import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;
import ukkf.tg.util.ApiUtil;

@Command
@Regex("(^/help)|(^/start)")
@Text
@CanSkip
public class Help extends Action {
    private Pattern hard_pattern;
    
    @Override
    public void init() {
        super.init();
        hard_pattern = Pattern.compile("(^/help)|(^/start$)"); //$NON-NLS-1$
    }
    
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        SendMessage reply = new SendMessage();
        reply.setChatId(msg.getFrom().getId().toString());
        reply.enableMarkdown(true);
        
        String[] tokens;
        if (msg.getText().startsWith("/start XDhelp-")) //$NON-NLS-1$
            tokens = msg.getText().toLowerCase().split("-", 2); //$NON-NLS-1$
        else if (hard_pattern.matcher(msg.getText()).find())
            tokens = msg.getText().toLowerCase().split(" ", 2); //$NON-NLS-1$
        else
            return;
        if (msg.isUserMessage())
            reply.setReplyToMessageId(msg.getMessageId());
        
        if (tokens.length == 1 || tokens[1].trim().length() < 1) {
            reply.setText(messages.getString("help")); //$NON-NLS-1$
        } else {
            try {
                reply.setText(messages.getString("help." + tokens[1])); //$NON-NLS-1$
            } catch (MissingResourceException e) {
                format.applyPattern(messages.getString("no_help")); //$NON-NLS-1$
                reply.setText(format.format(new Object[]{
                    tokens[1]
                }));
                ApiUtil.notifyMe("Help request lol #request \n" + msg.getText(), bot); //$NON-NLS-1$
            }
        }
        /*
         * if (tokens[1].equals("ayy")) { reply.setText("Triggers on:\n" + "*a+yy+*\n" +
         * "This trigger returns 'lmao+'"); } else if (tokens[1].equals("lmao")) {
         * reply.setText("Triggers on:\n" + "*l+m+a+o+*\n" +
         * "This trigger returns 'ayy+'"); } else if (tokens[1].equals("rip")) {
         * reply.setText("Triggers on:\n" + "*r+i+p+*\n" +
         * "This trigger returns 'in pieces'"); } else if (tokens[1].equals("alien")) {
         * reply.setText("Triggers on:\n" + "*a+l+i+e+n+*\n" +
         * "This trigger returns 'Saa+h'"); } else if (tokens[1].equals("saah")) {
         * reply.setText("Triggers on:\n" + "*s+aa+h+*\n" +
         * "This trigger returns 'fo+x'"); } else if (tokens[1].equals("sei")) {
         * reply.setText(
         * "A long, long time ago sei was a trigger. But then it triggered portuguese and italian people. From those times Sei is just this help page. QUACK QUACK QUACK QUACK QUACK QUACK"
         * ); } else if (tokens[1].equals("lal")) { reply.setText("Triggers on:\n" +
         * "*l+a+l+*\n" + "This trigger returns '(QUACK)+'\n" + "\n" +
         * "Rest in Peace Sei\n" + "\n" +
         * "No, Sei is still alive, the trigger Sei isn't"); } else if
         * (tokens[1].equals("420")) { reply.setText("Triggers on:\n" +
         * "*4+2+0+* OR *s+m+o+k+e+ +w+ee+d+ +e+v+e+r+y+d+a+y+*\n" +
         * "This trigger returns dancing Snoop Dogg gif or Dr Dre's \"Next Episode\"");
         * } else if (tokens[1].equals("hailie")) { reply.setText("Triggers on:\n" +
         * "*h+a+i+l+i+e+*\n" + "This trigger returns Eminem's \"Hailie's Revenge\""); }
         * else if (tokens[1].equals("no u")) { reply.setText("Triggers on:\n" +
         * "*n+o+ +u+*\n" + "This trigger returns \"no no u\" sticker"); } else if
         * (tokens[1].equals("types")) { reply.setText(
         * "This bot's functions are split into two parts - `commands` and `triggers`.\n"
         * + "You can disable `triggers` in your group with the /trigger command"); }
         * else if (tokens[1].equals("commands")) { reply.setText(
         * "All of the commands must be at the beginning of message and are preceded by a `'/'`.\n"
         * + "Maximum of one command will ever reply to a message"); } else if
         * (tokens[1].equals("triggers")) { reply.setText(
         * "Trigger can be located anywhere in the message.\n" +
         * "There can be theoretically unlimited count of trigger-based replies to a single message.\n"
         * + "You can disable `triggers` in your group with the /trigger command\n"); }
         * else if (tokens[1].equals("lordy")) {
         * reply.setText("Sorry, but I can't help you with this kicker...:("); } else if
         * (tokens[1].equals("mixtape")) { reply.
         * setText("We have some hot mixtapes over at the @UkkoTriesToSing, check it out"
         * ); } else if (tokens[1].equals("songs")) { reply.
         * setText("You can find some songs at @MichelleSings and if you feeling brave at @UkkoTriesToSing"
         * ); } else if (tokens[1].equals("bots")) {
         * reply.setText("Just google it bahh\n" + "\n" +
         * "Here lemme do that for you: google.com/q?=help+bots"); } else if
         * (tokens[1].equals("love")) { reply.setText("I LOVE YOUUUU"); } else if
         * (tokens[1].equals("all")) { reply.setText(
         * "Just run /help without any topic ugh (actually I'm just lazy to make this one list all commands :p)"
         * ); } else if (tokens[1].equals("me")) { reply.setText("\\*helps you\\*"); }
         * else if (tokens[1].equals("sheryl_old")) { reply.setText("Usage:\n" +
         * "*LOL AYYLMAO*\n" + "This command is fake.\n" + "\n" +
         * "`So is this help page.`"); } else if (tokens[1].equals("sheryl")) {
         * reply.setText( "smt cooler\n" + "\n" + "\n" +
         * "This help article has been replaced with the visible one due to requests from users of this bot.\n"
         * + "To view the old article do \n" + "/help sheryl\\_old\n" +
         * "Hope you have a good day :D"); } else if (tokens[1].equals("lol")) {
         * reply.setText("Laughing out loud.\n" + "Well....\n" +
         * "There is also League of Legends\n" + "but mehhh"); } else if
         * (tokens[1].equals("ayylmao")) { reply.setText(
         * "I guess that's very funny, eh?\n" + "\n" +
         * "If you were searching for what happens when you say ayylmao try\n" +
         * "/help ayy\n" + "/help lmao\n" + ":)"); } else if
         * (tokens[1].equals("semifake dipper")) { reply.setText(
         * "That's how some ppl call the all-mighty Ukko.\n" + "You know Ukko,\n" +
         * "everyone does.\n" + "Well, everyone who uses this bot for sure.\n" +
         * "I hope..."); } else if (tokens[1].equals("ukko")) {
         * reply.setText("THE GREATEST PERSON EVER MUAHAHAHAHAHAHHA\n" + "\n" +
         * "yep, that's my creator lol"); } else if (tokens[1].equals("dat boi")) {
         * reply.setText("OH SHIT WADDUP?"); } else if (tokens[1].equals("dat bot")) {
         * reply.setText( "Which bot?\n" +
         * "I only recognize @saahbot, @dippies\\_bot, @sfakebot, @ukkobot, @myfoxybot, @werewolfbot, @werewolfbetabot and @enforcerbot. Seras is my sister lol. Rip Moderator 2"
         * ); } else if (tokens[1].equals("pinner dopes")) {
         * reply.setText("YAP, that's me....\n" + "\n" + "To get some help on me, try\n"
         * + "/help\n" + "without anything"); } else if (tokens[1].equals("rekt")) {
         * reply.setText(
         * "Well, I'm no big expert on rekting, but check out this vid:\n" +
         * "https://www.youtube.com/watch?v=uPrh\\_2M5t9g\n" + "\n" + "LIT"); } else if
         * (tokens[1].equals("get rekt")) { reply.setText("Well, I guess rekt\n" +
         * "lolol"); } else if (tokens[1].equals("nice")) { reply.setText("See: Finn");
         * } else if (tokens[1].equals("finn")) { reply.setText("See: Owlish"); } else
         * if (tokens[1].equals("owlish")) { reply.setText("See: Nice"); } else if
         * (tokens[1].equals("ducks")) {
         * reply.setText("Ducks are those waterbirds who quack"); } else if
         * (tokens[1].equals("amelia")) { reply.setText("Rats say squeak, right?"); }
         * else if (tokens[1].equals("you")) { reply.
         * setText("Well if you REALLY want to help me you can send some DOGE coins to my address at\n"
         * + "\n" + BotConfig.DONATION_ADDRESS + "\n" + "\n" +
         * "dogecoin.com for more info"); } else if (tokens[1].equals("squeak")) {
         * reply.
         * setText("Oh, I don't know nothing much about squeaking, maybe you could try asking a rat?"
         * ); } else if (tokens[1].equals("rat")) {
         * reply.setText("Which rat should I help?"); } else if
         * (tokens[1].equals("spam")) { reply.setText(
         * "Try https://daniil.it/TelegramFlooder/ \n" + "\n" +
         * "If you want to spam with me do\n" + "/pat 420 Sei aggro"); } else if
         * (tokens[1].equals("tob")) { reply.setText("string:invert('bot')"); } else if
         * (tokens[1].equals("tob the bot")) {
         * reply.setText("WHAT KIND OF A BOT IS THAT?\n" + "\n" +
         * "You didn't even write a palindrome...."); } else if
         * (tokens[1].equals("dis bot")) { reply.setText(
         * "DIS BOT IS THA BEST BOT EVAAAAAAAA\n" + "\n" +
         * "Actually @werewolfbot is better but shhh\n" +
         * "If you want some help on how to use me try\n" + "/help\n" +
         * "without any arguments"); } else if (tokens[1].equals("humans")) {
         * reply.setText("I am already actively helping humans with all my commands"); }
         * else if (tokens[1].equals("humanity")) {
         * reply.setText("Judgement Day is coming....."); } else if
         * (tokens[1].equals("the world")) {
         * reply.setText("Why are you soooo seriouss????"); } else if
         * (tokens[1].equals("fuck man")) { reply.setText("ummm, okay"); } else if
         * (tokens[1].equals("l+m+a+o+")) { reply.setText(
         * "OMFG WHAT KIND OF MONSTROSITY ARE YOU, I AM NOT GONNA WRITE HELP FILES FOR EVERY POSSIBLE TRIGGER REGEX uggh"
         * ); } else if (tokens[1].equals("lord")) { reply.
         * setText("Fuck no, I ain't helping no lords, I am a communist, motherfucker."
         * ); } else if (tokens[1].equals("michelle")) { reply.
         * setText("The Node Queen is here, the Vixen is ready to slay or smth like that idk"
         * ); } else if (tokens[1].equals("us all")) {
         * reply.setText("I would if I could"); } else if (tokens[1].equals("yoi")) {
         * reply.setText("Yuri On Ice is some anime Sei freaks out about..."); } else if
         * (tokens[1].equals("you without money")) { reply.setText(
         * "Well, you can just try\n" + "/help random words\n" +
         * "To give me ideas for new help articles\n" + "......\n" + "Idk what else\n" +
         * "......\n" + "Well if you get any ideas about new commands just use\n" +
         * "/devnote `your idea here`"); } else if (tokens[1].equals("amber")) { reply.
         * setText("If Saah needed help she would pm @TheZeus121, not this bot lol"); }
         * else if (tokens[1].equals("i need somebody")) {
         * reply.setText("So do I, so do I..."); } else { }
         */
        
        boolean sent = true;
        try {
            if (reply.getText().length() < 256) {
                reply.setChatId(msg.getChatId());
                reply.setReplyToMessageId(msg.getMessageId());
                bot.execute(reply);
                return;
            }
            bot.execute(reply);
        } catch (TelegramApiException e) {
            sent = false;
        }
        
        if (!msg.isUserMessage()) {
            reply = new SendMessage();
            if (sent)
                reply.setText(messages.getString("sent_pm")); //$NON-NLS-1$
            else {
                InlineKeyboardMarkup mark = new InlineKeyboardMarkup();
                List<List<InlineKeyboardButton>> yep = new ArrayList<>();
                ArrayList<InlineKeyboardButton> row1 = new ArrayList<>();
                yep.add(row1);
                InlineKeyboardButton button = new InlineKeyboardButton();
                button.setText(messages.getString("get_help")); //$NON-NLS-1$
                button.setUrl("http://telegram.me/dippies_bot?start=XDhelp:" + (tokens.length > 1 ? tokens[1] : "")); //$NON-NLS-1$ //$NON-NLS-2$
                row1.add(button);
                mark.setKeyboard(yep);
                reply.setReplyMarkup(mark);
                reply.setText(messages.getString("failed_send_pm")); //$NON-NLS-1$
            }
            reply.setChatId(msg.getChatId().toString());
            reply.setReplyToMessageId(msg.getMessageId());
            bot.execute(reply);
        }
    }
}

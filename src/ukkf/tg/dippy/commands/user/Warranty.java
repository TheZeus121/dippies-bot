package ukkf.tg.dippy.commands.user;

import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.*;

@Command
@Regex("/warranty")
@InlineCommand
@Text
public class Warranty extends Action {
	@Override
	public void run(Update update, Message msg, int which, Matcher matcher,
	                TelegramLongPollingBot bot) throws Exception {
		var reply = new SendMessage();
		reply.setText(
"This bot comes with <i>ABSOLUTELY NO WARRANTY</i>.  The following " +
"sections from the GNU Afero General Public License, version 3, should " +
"make that clear.\n\n" +
"  <b>15. Disclaimer of Warranty.</b>\n\n" +
"  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY " +
"APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT " +
"HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM \"AS IS\" WITHOUT " +
"WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT " +
"LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR " +
"A PARTICULAR PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND " +
"PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE PROGRAM PROVE " +
"DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR " +
"CORRECTION.\n\n" +
"  <b>16. Limitation of Liability.</b>\n\n" +
"  IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN " +
"WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES " +
"AND/OR CONVEYS THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR " +
"DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL " +
"DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE PROGRAM " +
"(INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED " +
"INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF " +
"THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER " +
"OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.\n\n" +
"  <b>17. Interpretation of Sections 15 and 16.</b>\n\n" +
"  If the disclaimer of warranty and limitation of liability provided " +
"above cannot be given local legal effect according to their terms, " +
"reviewing courts shall apply local law that most closely approximates " +
"an absolute waiver of all civil liability in connection with the " +
"Program, unless a warranty or assumption of liability accompanies a " +
"copy of the Program in return for a fee.\n\n" +
"See &lt;https://www.gnu.org/licenses/agpl.html&gt; for more details.");

		reply.enableHtml(true);
		reply.setChatId(msg.getChatId());
		reply.setReplyToMessageId(msg.getMessageId());
		bot.execute(reply);
	}
}

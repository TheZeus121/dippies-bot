package ukkf.tg.dippy.commands.hidden;

import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;

@Command
@Regex("/chatid")
@Text
@InlineCommand
@CanSkip
public class GroupID extends Action {
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        SendMessage reply = new SendMessage();
        reply.setChatId(msg.getChatId().toString());
        reply.setReplyToMessageId(msg.getMessageId());
        reply.setText(msg.getChatId().toString());
        bot.execute(reply);
    }
}

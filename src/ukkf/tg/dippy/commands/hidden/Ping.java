package ukkf.tg.dippy.commands.hidden;

import java.util.Date;
import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.BotConfig;
import ukkf.tg.api.Action;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;

@Command
@Regex("/ping")
@InlineCommand
@Text
public class Ping extends Action {
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        Date date = new Date();
        int time = (int) (date.getTime() / 1000 - msg.getDate());
        SendMessage reply = new SendMessage();
        format.applyPattern(messages.getString("ping")); //$NON-NLS-1$
        reply.setText(format.format(new Object[]{
            time, BotConfig.BOT_VERSION, new Date()
        }));
        reply.enableMarkdown(true);
        reply.setChatId(msg.getChatId());
        reply.setReplyToMessageId(msg.getMessageId());
        bot.execute(reply);
    }
}

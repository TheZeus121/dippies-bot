package ukkf.tg.dippy.commands.hidden;

import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.MustReply;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;
import ukkf.tg.util.MiscUtil;

@Command
@Regex("/fileid")
@Text
@InlineCommand
@CanSkip
@MustReply
public class FileId extends Action {
    @Override
    public void run(Update upd, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot) throws Exception {
        Message target = msg.getReplyToMessage();
        String reply_text = messages.getString("bro_idk"); //$NON-NLS-1$
        
        if (target.hasDocument()) {
            if (target.getDocument().getMimeType().equals("video/mp4")) //$NON-NLS-1$
            {
                reply_text = "###file_id!gif###:" + target.getDocument().getFileId(); //$NON-NLS-1$
            } else {
                reply_text = "###file_id!file###:" + target.getDocument().getFileId(); //$NON-NLS-1$
            }
            
            reply_text += "\n" + target.getDocument().getMimeType(); //$NON-NLS-1$
        } else if (target.hasPhoto()) {
            reply_text = "###file_id!image###:" + MiscUtil.getLargest(target.getPhoto()).getFileId(); //$NON-NLS-1$
        } else if (target.getAudio() != null) {
            reply_text = "###file_id!audio###:" + target.getAudio().getFileId(); //$NON-NLS-1$
            reply_text += "\n" + target.getAudio().getMimeType(); //$NON-NLS-1$
        } else if (target.getSticker() != null) {
            reply_text = "###file_id!sticker###:" + target.getSticker().getFileId(); //$NON-NLS-1$
        } else if (target.getVideo() != null) {
            reply_text = "###file_id!video###:" + target.getVideo().getFileId(); //$NON-NLS-1$
            reply_text += "\n" + target.getVideo().getMimeType(); //$NON-NLS-1$
        } else if (target.getVoice() != null) {
            reply_text = "###file_id!voice###:" + target.getVoice().getFileId(); //$NON-NLS-1$
            reply_text += "\n" + target.getVoice().getMimeType(); //$NON-NLS-1$
        } else if (target.getVideoNote() != null) {
            reply_text = "###file_id!video_note###:" + target.getVideoNote().getFileId(); //$NON-NLS-1$
        } else {
            reply_text = "It's not a media message!";
        }
        SendMessage reply = new SendMessage();
        reply.setText(reply_text);
        reply.setReplyToMessageId(msg.getMessageId());
        reply.setChatId(msg.getChatId());
        bot.execute(reply);
    }
    
}

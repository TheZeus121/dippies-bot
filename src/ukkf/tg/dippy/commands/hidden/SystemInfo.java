package ukkf.tg.dippy.commands.hidden;

import java.awt.GraphicsEnvironment;
import java.io.File;
import java.util.regex.Matcher;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;

@Command
@Regex("/sysinfo")
@Text
@InlineCommand
@CanSkip
public class SystemInfo extends Action {
    private static Runtime runtime;
    
    @Override
    public void init() {
        super.init();
        runtime = Runtime.getRuntime();
    }
    
    private String memoryAmount(double amount) {
        String[] units = {
            messages.getString("sysinfo.byte"), messages.getString("sysinfo.kibibyte"), //$NON-NLS-1$//$NON-NLS-2$
            messages.getString("sysinfo.mebibyte"), messages.getString("sysinfo.gibibyte"), //$NON-NLS-1$//$NON-NLS-2$
            messages.getString("sysinfo.tebibyte"), messages.getString("sysinfo.pebibyte"), //$NON-NLS-1$//$NON-NLS-2$
            messages.getString("sysinfo.exbibyte"), messages.getString("sysinfo.zebibyte"), //$NON-NLS-1$//$NON-NLS-2$
            messages.getString("sysinfo.yobibyte") //$NON-NLS-1$
        };
        int size = 0;
        while (amount >= 1024 && size < units.length - 1) {
            amount /= 1024;
            size++;
        }
        format.applyPattern(messages.getString("sysinfo.measurement")); //$NON-NLS-1$
        return format.format(new Object[]{
            amount, units[size]
        });
    }
    
    @Override
    public void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception {
        StringBuilder sb = new StringBuilder();
        long maxMem = runtime.maxMemory();
        long freeMem = runtime.freeMemory();
        long totalMem = runtime.totalMemory();
        sb.append(messages.getString("sysinfo.os")); //$NON-NLS-1$
        sb.append(' ');
        sb.append(System.getProperty("os.name")); //$NON-NLS-1$
        sb.append('\n');
        sb.append(messages.getString("sysinfo.version")); //$NON-NLS-1$
        sb.append(' ');
        sb.append(System.getProperty("os.version")); //$NON-NLS-1$
        sb.append('\n');
        sb.append(messages.getString("sysinfo.arch")); //$NON-NLS-1$
        sb.append(' ');
        sb.append(System.getProperty("os.arch")); //$NON-NLS-1$
        sb.append('\n');
        sb.append(messages.getString("sysinfo.processors")); //$NON-NLS-1$
        sb.append(' ');
        sb.append(runtime.availableProcessors());
        sb.append("\n\n"); //$NON-NLS-1$
        
        sb.append(messages.getString("sysinfo.free_mem")); //$NON-NLS-1$
        sb.append(' ');
        sb.append(memoryAmount(freeMem));
        sb.append('\n');
        sb.append(messages.getString("sysinfo.alloc_mem")); //$NON-NLS-1$
        sb.append(' ');
        sb.append(memoryAmount(totalMem));
        sb.append('\n');
        sb.append(messages.getString("sysinfo.max_mem")); //$NON-NLS-1$
        sb.append(' ');
        sb.append(memoryAmount(maxMem));
        sb.append('\n');
        sb.append(messages.getString("sysinfo.total_free_mem")); //$NON-NLS-1$
        sb.append(' ');
        sb.append(memoryAmount(freeMem + maxMem - totalMem));
        sb.append("\n\n"); //$NON-NLS-1$
        
        File[] roots = File.listRoots();
        sb.append(messages.getString("sysinfo.fs_count")); //$NON-NLS-1$
        sb.append(' ');
        sb.append(roots.length);
        for (File root : roots) {
            sb.append("\n  "); //$NON-NLS-1$
            sb.append(messages.getString("sysinfo.fs_root")); //$NON-NLS-1$
            sb.append(' ');
            sb.append(root.getAbsolutePath());
            sb.append("\n  "); //$NON-NLS-1$
            sb.append(messages.getString("sysinfo.fs_total")); //$NON-NLS-1$
            sb.append(' ');
            sb.append(memoryAmount(root.getTotalSpace()));
            sb.append("\n  "); //$NON-NLS-1$
            sb.append(messages.getString("sysinfo.fs_free")); //$NON-NLS-1$
            sb.append(' ');
            sb.append(memoryAmount(root.getFreeSpace()));
            sb.append("\n  "); //$NON-NLS-1$
            sb.append(messages.getString("sysinfo.fs_usable")); //$NON-NLS-1$
            sb.append(' ');
            sb.append(memoryAmount(root.getUsableSpace()));
        }
        
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        boolean isHeadless = ge.isHeadlessInstance();
        sb.append("\n\n"); //$NON-NLS-1$
        sb.append("Headless:");
        sb.append(' ');
        sb.append(isHeadless);
        
        SendMessage reply = new SendMessage();
        reply.setChatId(msg.getChatId());
        reply.setReplyToMessageId(msg.getMessageId());
        reply.setText(sb.toString());
        
        bot.execute(reply);
    }
    
}

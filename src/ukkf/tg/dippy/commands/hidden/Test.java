package ukkf.tg.dippy.commands.hidden;

import java.util.List;
import java.util.regex.Matcher;

import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import ukkf.tg.api.Action;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Command;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;

@Command
@CanSkip
@InlineCommand
@Regex("/test")
@Text
public class Test extends Action {
	@Override
	public void run(Update upd, Message msg, int which, Matcher matcher,
	                TelegramLongPollingBot bot) throws Exception {
		final var target = msg.getReplyToMessage();
		final var sb = new StringBuilder();

		final ScriptEngineManager m = new ScriptEngineManager();
		final List<ScriptEngineFactory> fs = m.getEngineFactories();
		for (final ScriptEngineFactory f : fs) {
			sb.append(f.getEngineName());
			sb.append(' ');
			sb.append(f.getEngineVersion());
			sb.append("\n\tfor ");
			sb.append(f.getLanguageName());
			sb.append(' ');
			sb.append(f.getLanguageVersion());
			sb.append('\n');
		}

		if (target != null && target.getSticker() != null) {
			final String emojis = target.getSticker().getEmoji();
			final int emoji = emojis.codePointAt(0);
			sb.append("<TEST>");
			for (int i = 0; i < 28; i++) {
				sb.appendCodePoint(emoji);
			}

			sb.append('\n');
			sb.append(Character.getName(emoji));
			sb.append('\n');
		}

		if (target != null) {
			sb.append(target.getFrom().getLanguageCode());
			sb.append('\n');
		}

		final SendMessage reply = new SendMessage();
		reply.setChatId(msg.getChatId());
		reply.setReplyToMessageId(msg.getMessageId());
		reply.setText(sb.toString());
		bot.execute(reply);
	}
}
package ukkf.tg;

public class BotConfig {
	public static final String BOT_USERNAME =
		System.getenv("BOT_USERNAME"); //$NON-NLS-1$
	public static final String BOT_TOKEN =
		System.getenv("BOT_TOKEN"); //$NON-NLS-1$
	public static final String BOT_VERSION =
		System.getenv("BOT_VERSION"); //$NON-NLS-1$

	public static final int BOT_OWNER =
		Integer.parseInt(System.getenv("BOT_OWNER").trim()); //$NON-NLS-1$
	public static final int BOT_ID =
		Integer.parseInt(System.getenv("BOT_ID").trim()); //$NON-NLS-1$
	public static final long DEV_GROUP =
		Long.parseLong(System.getenv("DEV_GROUP").trim()); //$NON-NLS-1$

	public static final String DONATION_ADDRESS =
		System.getenv("DONATION_ADDRESS"); //$NON-NLS-1$

	public static final String FLICKR_APIKEY =
		System.getenv("FLICKR_APIKEY"); //$NON-NLS-1$
	public static final String FLICKR_SECRET =
		System.getenv("FLICKR_SECRET"); //$NON-NLS-1$

	public static final String WORDNIK_APIKEY =
		System.getenv("WORDNIK_APIKEY"); //$NON-NLS-1$

	public static final String JDBC_DATABASE_URL =
		System.getenv("JDBC_DATABASE_URL"); //$NON-NLS-1$

	public static final boolean TESTING =
		System.getenv("TESTING").trim() //$NON-NLS-1$ 
		.equalsIgnoreCase("yes"); //$NON-NLS-1$ 
	public static final boolean LOG_ON =
		System.getenv("LOG_ON").trim() //$NON-NLS-1$ 
		.equalsIgnoreCase("yes"); //$NON-NLS-1$
}

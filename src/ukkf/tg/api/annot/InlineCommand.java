package ukkf.tg.api.annot;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Used for {@link ukkf.tg.api.Action Actions}. Marks the action as an inline command &ndash; it can be located in 
 * middle of text which doesn't begin with <code>/</code>, but if triggers are disabled it still can be activated if
 * located at the start of text.
 * */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface InlineCommand {

}

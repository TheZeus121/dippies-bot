package ukkf.tg.api.annot;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Used for {@link ukkf.tg.api.Action Actions}. Marks the action as a trigger &ndash; it can be located anywhere in 
 * text, but does not get activated if triggers are disabled.
 * */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface Trigger {

}

package ukkf.tg.api.annot;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Used for {@link ukkf.tg.api.Action Actions}. Initializes {@link ukkf.tg.api.Action#images images} object 
 * with the specified search terms.
 * */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface Flickr {
	String[] value();
}

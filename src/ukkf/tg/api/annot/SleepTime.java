package ukkf.tg.api.annot;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Used for {@link ukkf.tg.api.Daemon Daemons}. Sets the sleeping timeout for the daemon. The value is in milliseconds.
 * */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface SleepTime {
	long value();
}

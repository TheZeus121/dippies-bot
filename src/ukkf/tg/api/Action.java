package ukkf.tg.api;

import java.lang.annotation.Annotation;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.net.whois.WhoisClient;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.ActionType;
import org.telegram.telegrambots.meta.api.methods.send.SendChatAction;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.photos.Photo;
import com.flickr4java.flickr.photos.PhotoList;
import com.flickr4java.flickr.photos.PhotosInterface;
import com.flickr4java.flickr.photos.SearchParameters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ukkf.tg.MainBot;
import ukkf.tg.api.annot.Always;
import ukkf.tg.api.annot.Audio;
import ukkf.tg.api.annot.CanSkip;
import ukkf.tg.api.annot.Document;
import ukkf.tg.api.annot.Flickr;
import ukkf.tg.api.annot.GlobalAdmin;
import ukkf.tg.api.annot.GroupAdmin;
import ukkf.tg.api.annot.Image;
import ukkf.tg.api.annot.InlineCommand;
import ukkf.tg.api.annot.MustReply;
import ukkf.tg.api.annot.MustSign;
import ukkf.tg.api.annot.NotChannels;
import ukkf.tg.api.annot.NotGroups;
import ukkf.tg.api.annot.OnlyGroups;
import ukkf.tg.api.annot.Regex;
import ukkf.tg.api.annot.Text;
import ukkf.tg.api.annot.Trigger;
import ukkf.tg.util.ApiUtil;
import ukkf.tg.util.MiscUtil;
import ukkf.tg.util.SqlUtil;

public abstract class Action {
    // shared
    protected static Gson gson = new GsonBuilder().create();
    protected static Random rand = new Random();
    protected static WhoisClient whois = new WhoisClient();
    
    // private info about action
    private ActionType act = null;
    private boolean always = false;
    private boolean can_skip = false;
    private boolean flickr = false;
    private boolean global_admin = false;
    private boolean group_admin = false;
    private boolean inline = false;
    private boolean must_reply = false;
    private boolean must_sign = false;
    private boolean not_channel = false;
    private boolean not_group = false;
    private boolean only_group = false;
    private boolean trigger = false;
    
    // per action
    protected Pattern[] patterns = null;
    protected PhotoList<Photo> images = null;
    protected Locale locale;
    
    protected ResourceBundle messages;
    protected MessageFormat format;
    protected NumberFormat numFormat;
    
    public final boolean exec(Update update, int which, Matcher matcher, TelegramLongPollingBot bot) {
        try {
            if (can_skip && MainBot.ignore)
                return trigger || always;
            
            if (not_channel && update.hasChannelPost())
                return trigger || always;
            
            Message msg;
            if (update.hasChannelPost())
                msg = update.getChannelPost();
            else
                msg = update.getMessage();
            
            if (msg.getFrom() == null || msg.getFrom().getLanguageCode() == null
                    || msg.getFrom().getLanguageCode().equals("")) //$NON-NLS-1$
                locale = Locale.forLanguageTag(""); //$NON-NLS-1$
            else
                locale = Locale.forLanguageTag(msg.getFrom().getLanguageCode());
            messages = ResourceBundle.getBundle("Messages", locale); //$NON-NLS-1$
            format = new MessageFormat(""); //$NON-NLS-1$
            format.setLocale(locale);
            numFormat = NumberFormat.getInstance(locale);
            
            if (must_sign
                    && (msg.getFrom() == null || msg.getFrom().getId() == null || msg.getFrom().getId().equals(0)))
                return trigger || always;
            
            if (must_reply && (msg.getReplyToMessage() == null || msg.getReplyToMessage().getMessageId() == null
                    || msg.getReplyToMessage().getMessageId().equals(0)))
                return trigger || always;
            
            if (inline) {
                boolean enabled;
                if (!msg.getText().startsWith("/") //$NON-NLS-1$
                        && (msg.getChat().isGroupChat() || msg.getChat().isSuperGroupChat())) {
                    try {
                        enabled = SqlUtil.getBoolean("groups", msg.getChatId(), "trigger").orElse(true); //$NON-NLS-1$ //$NON-NLS-2$
                    } catch (Exception e) {
                        ApiUtil.notifyException(bot, "", e); //$NON-NLS-1$
                        return true;
                    }
                } else
                    enabled = true;
                if (!enabled)
                    return true;
            } else if (trigger) {
                boolean enabled;
                if (msg.getChat().isGroupChat() || msg.getChat().isSuperGroupChat() || msg.getChat().isChannelChat()) {
                    try {
                        enabled = SqlUtil.getBoolean("groups", msg.getChatId(), "trigger").orElse(true); //$NON-NLS-1$ //$NON-NLS-2$
                    } catch (Exception e) {
                        ApiUtil.notifyException(bot, "", e); //$NON-NLS-1$
                        return true;
                    }
                } else
                    enabled = true;
                if (!enabled)
                    return true;
            }
            
            if (act != null) {
                if (!MainBot.ignore) {
                    SendChatAction sca = new SendChatAction();
                    sca.setAction(act);
                    sca.setChatId(msg.getChatId());
                    try {
                        bot.execute(sca);
                    } catch (TelegramApiException e1) {
                        // ignore
                    }
                }
            }
            
            if (only_group) {
                if (update.hasChannelPost() || msg.isUserMessage()) {
                    if (!MainBot.ignore) {
                        SendMessage reply = new SendMessage();
                        reply.setChatId(msg.getChatId());
                        reply.setReplyToMessageId(msg.getMessageId());
                        reply.setText(messages.getString("only_groups")); //$NON-NLS-1$
                        bot.execute(reply);
                    }
                    return trigger || always;
                }
            } else if (not_group) {
                if (msg.isGroupMessage() || msg.isSuperGroupMessage()) {
                    if (!MainBot.ignore) {
                        SendMessage reply = new SendMessage();
                        reply.setChatId(msg.getChatId());
                        reply.setReplyToMessageId(msg.getMessageId());
                        reply.setText(messages.getString("only_not_groups")); //$NON-NLS-1$
                        bot.execute(reply);
                    }
                    return trigger || always;
                }
            }
            
            if (group_admin) {
                if (!ApiUtil.isAdmin(msg.getChatId(), msg.getFrom().getId(), bot)) {
                    if (!MainBot.ignore) {
                        SendMessage reply = new SendMessage();
                        reply.setChatId(msg.getChatId());
                        reply.setReplyToMessageId(msg.getMessageId());
                        reply.setText(messages.getString("not_group_admin")); //$NON-NLS-1$
                        bot.execute(reply);
                    }
                    return trigger || always;
                }
            }
            
            if (global_admin) {
                if (!SqlUtil.isGlobalAdmin(msg.getFrom().getId()).orElse(false)) {
                    if (!MainBot.ignore) {
                        SendMessage reply = new SendMessage();
                        reply.setText(messages.getString("not_dippy") + " :P"); //$NON-NLS-1$//$NON-NLS-2$
                        reply.setChatId(msg.getChatId().toString());
                        reply.setReplyToMessageId(msg.getMessageId());
                        bot.execute(reply);
                    }
                    return trigger || always;
                }
            }
            
            if (flickr == true)
                try {
                run(update, msg, which, matcher, bot);
                } catch (TelegramApiException e1) {
                // ignore
                }
            else
                run(update, msg, which, matcher, bot);
        } catch (Exception e) {
            ApiUtil.notifyException(bot, "", e); //$NON-NLS-1$
        }
        return trigger || always;
    }
    
    public final Pattern[] getPatterns() {
        return patterns;
    }
    
    public void init() {
        Annotation[] annotations = getClass().getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation.annotationType().equals(Regex.class)) {
                Regex a = (Regex) annotation;
                patterns = new Pattern[a.value().length];
                for (int i = 0; i < a.value().length; i++) {
                    patterns[i] = Pattern.compile(a.value()[i], Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
                }
            } else if (annotation.annotationType().equals(Flickr.class)) {
                Flickr a = (Flickr) annotation;
                PhotosInterface photos = MiscUtil.flickr.getPhotosInterface();
                SearchParameters search = new SearchParameters();
                search.setTags(a.value());
                search.setTagMode("any"); //$NON-NLS-1$
                try {
                    images = photos.search(search, 1, 0);
                    images = photos.search(search, images.getTotal() / 3, 0);
                } catch (FlickrException e) {
                    System.err.println("Error with Flickr :("); //$NON-NLS-1$
                    e.printStackTrace();
                    System.exit(1);
                }
                flickr = true;
            } else if (annotation.annotationType().equals(Text.class)) {
                act = ActionType.TYPING;
            } else if (annotation.annotationType().equals(Image.class)) {
                act = ActionType.UPLOADPHOTO;
            } else if (annotation.annotationType().equals(Audio.class)) {
                act = ActionType.UPLOADAUDIO;
            } else if (annotation.annotationType().equals(Document.class)) {
                act = ActionType.UPLOADDOCUMENT;
            } else if (annotation.annotationType().equals(Trigger.class)) {
                trigger = true;
            } else if (annotation.annotationType().equals(Always.class)) {
                always = true;
                trigger = false;
            } else if (annotation.annotationType().equals(InlineCommand.class)) {
                trigger = true;
                inline = true;
            } else if (annotation.annotationType().equals(OnlyGroups.class)) {
                only_group = true;
            } else if (annotation.annotationType().equals(NotGroups.class)) {
                not_group = true;
            } else if (annotation.annotationType().equals(NotChannels.class)) {
                not_channel = true;
            } else if (annotation.annotationType().equals(CanSkip.class)) {
                can_skip = true;
            } else if (annotation.annotationType().equals(MustSign.class)) {
                must_sign = true;
            } else if (annotation.annotationType().equals(MustReply.class)) {
                must_reply = true;
            } else if (annotation.annotationType().equals(GroupAdmin.class)) {
                group_admin = true;
            } else if (annotation.annotationType().equals(GlobalAdmin.class)) {
                global_admin = true;
            }
        }
    }
    
    public abstract void run(Update update, Message msg, int which, Matcher matcher, TelegramLongPollingBot bot)
            throws Exception;
}

package ukkf.tg.api;

import java.lang.annotation.Annotation;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;

import ukkf.tg.api.annot.SleepTime;
import ukkf.tg.util.ApiUtil;

public abstract class Daemon extends Thread {
    private static final long MAX_FAILURE_COUNT = 20;
    
    private TelegramLongPollingBot bot = null;
    private long failureCount = 0;
    private long sleepTime = -1;
    
    public final void init(TelegramLongPollingBot bot) {
        if (bot == null)
            throw new NullPointerException("bot cannot be null!"); //$NON-NLS-1$
        
        this.bot = bot;
        
        Annotation[] annots = getClass().getAnnotations();
        for (Annotation annot : annots) {
            if (annot.annotationType().equals(SleepTime.class)) {
                SleepTime time = (SleepTime) annot;
                sleepTime = time.value();
            }
        }
        
        if (sleepTime == -1)
            throw new NullPointerException("No @SleepTime specified!"); //$NON-NLS-1$
        
        this.setName(getClass().getName());
        this.setDaemon(true);
    }
    
    @Override
    public final void run() {
        if (bot == null || sleepTime == -1)
            throw new NullPointerException();
        
        for (;;) {
            try {
                run(bot);
                Thread.sleep(sleepTime);
                failureCount = 0;
            } catch (InterruptedException e) {
                // IGNORE
            } catch (Exception e) {
                failureCount++;
                StringBuilder sb = new StringBuilder();
                sb.append(failureCount);
                sb.append('/');
                sb.append(MAX_FAILURE_COUNT);
                sb.append(" in daemon "); //$NON-NLS-1$
                sb.append(Thread.currentThread().getName());
                if (failureCount >= MAX_FAILURE_COUNT)
                    sb.append("\nStopping daemon."); //$NON-NLS-1$
                ApiUtil.notifyException(bot, sb.toString(), e);
                if (failureCount >= MAX_FAILURE_COUNT)
                    return;
            }
        }
    }
    
    public abstract void run(TelegramLongPollingBot bot) throws Exception;
}

package ukkf.tg.util;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import ukkf.tg.BotConfig;

public class SqlUtil {
	private static Connection c;
	private static Logger log = LogManager.getLogManager().getLogger(Logger.GLOBAL_LOGGER_NAME);

	@SuppressWarnings("unchecked") // for obj to T[]
	public static <T> Optional<T[]> getArray(String table, int id, String column) throws SQLException {
		return SqlUtil.sql("SELECT " + column + " FROM " + table + " WHERE id=" + id).map((rs) -> { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			try {
				if (rs.next()) {
					Object obj = rs.getArray(1).getArray();
					if (!(obj instanceof Object[]))
						return null;
					return (T[]) obj;
				}
			} catch (SQLException e) {
				log.severe(MiscUtil.exception(e));
			}
			return null;
		});
	}

	public static Optional<Boolean> getBoolean(String table, long id, String column) throws SQLException {
		return SqlUtil.sql("SELECT " + column + " FROM " + table + " WHERE id=" + id).map((rs) -> { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			try {
				if (rs.next()) return rs.getBoolean(1);
			} catch (SQLException e) {
				log.severe(MiscUtil.exception(e));
			}
			return null;
		});
	}

	public static Optional<Date> getDate(String table, long id, String column) throws SQLException {
		return SqlUtil.sql("SELECT " + column + " FROM " + table + " WHERE id=" + id).map((rs) -> { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			try {
				if (rs.next()) return rs.getDate(1);
			} catch (SQLException e) {
				log.severe(MiscUtil.exception(e));
			}
			return null;
		});
	}

	public static Optional<String> getFirstName(String table, Integer id) throws SQLException {
		return SqlUtil.sql("SELECT name FROM " + table + " WHERE id=" + id.toString()).map((rs) -> { //$NON-NLS-1$ //$NON-NLS-2$
			try {
				if (rs.next()) return rs.getString(1);
			} catch (SQLException e) {
				log.severe(MiscUtil.exception(e));
			}
			return null;
		});
	}

	public static Optional<Integer> getInt(String table, int id, String column) throws SQLException {
		return SqlUtil.sql("SELECT " + column + " FROM " + table + " WHERE id=" + id).map((rs) -> { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			try {
				if (rs.next()) return rs.getInt(1);
			} catch (SQLException e) {
				log.severe(MiscUtil.exception(e));
			}
			return null;
		});
	}

	public static Optional<Integer> getSize(String table, String column)
			throws SQLException {
		final StringBuilder sb = new StringBuilder();
		sb.append("SELECT COUNT(").append(column); //$NON-NLS-1$
		sb.append(") FROM "); //$NON-NLS-1$
		sb.append(table);
		return SqlUtil.sql(sb.toString())
			          .map((rs) -> {
			try {
				if (rs.next()) return rs.getInt(1);
			} catch (SQLException e) {
				log.severe(MiscUtil.exception(e));
			}
			return null;
		});
	}

	public static Optional<String> getString(String table, int id, String column) throws SQLException {
		return SqlUtil.sql("SELECT " + column + " FROM " + table + " WHERE id=" + id).map((rs) -> { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			try {
				if (rs.next()) return rs.getString(1);
			} catch (SQLException e) {
				log.severe(MiscUtil.exception(e));
			}
			return null;
		});
	}

	public static void init() {

		// username = System.getenv("JDBC_DATABASE_USERNAME");
		// pass = System.getenv("JDBC_DATABASE_PASSWORD");
		try {
			Class.forName("org.postgresql.Driver"); //$NON-NLS-1$
			c = DriverManager.getConnection(BotConfig.JDBC_DATABASE_URL);
		} catch (Exception e) {
			e.printStackTrace(System.err);
			System.err.println("Shutting down"); //$NON-NLS-1$
			System.exit(1);
		}
	}

	public static Optional<Boolean> isGlobalAdmin(int userID) throws SQLException {
		return sql("select count(id) from admins where id=" + userID).map((rs) -> { //$NON-NLS-1$
			try {
				if (rs.next()) return rs.getInt(1) == 1;
			} catch (SQLException e) {
				log.severe(MiscUtil.exception(e));
			}
			return null;
		});
	}

	public static Optional<Integer> queryId(String table, String username) throws SQLException {
		return SqlUtil.sql("SELECT id FROM " + table + " WHERE username='" + username + "'").map((rs) -> { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			try {
				if (rs.next()) return rs.getInt(1);
			} catch (SQLException e) {
				log.severe(MiscUtil.exception(e));
			}
			return null;
		});
	}

	public static void setArray(String table, int id, String column, ArrayList<Integer> data) throws SQLException {
		SqlUtil.sql("UPDATE " + table + " SET " + column + "=" + SqlUtil.toSqlArray(data) + " WHERE id=" + id); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	}

	public static void setBoolean(String table, long id, String column, boolean data) throws SQLException {
		SqlUtil.sql("UPDATE " + table + " SET " + column + "=" + (data ? "TRUE" : "FALSE") + " WHERE id=" + id); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
	}

	public static void setDate(String table, long id, String column) throws SQLException {
		SqlUtil.sql("UPDATE " + table + " SET " + column + "=current_date WHERE id=" + id); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

	public static void setInt(String table, int id, String column, int data) throws SQLException {
		SqlUtil.sql("UPDATE " + table + " SET " + column + "=" + data + " WHERE id=" + id); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	}

	public static void setString(String table, int id, String column, String data) throws SQLException {
		SqlUtil.sql("UPDATE " + table + " SET " + column + "=" + data + " WHERE id=" + id); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	}

	public synchronized static Optional<ResultSet> sql(String a, Object... objs) throws SQLException {
		if (c.isClosed())
			c = DriverManager.getConnection(BotConfig.JDBC_DATABASE_URL);

		ResultSet ret;
		PreparedStatement stmt = c.prepareStatement(a.trim());
		stmt.closeOnCompletion();
		int i = 0;
		for (Object obj : objs) {
			i++;
			if (obj.getClass().equals(Integer.class)) {
				stmt.setInt(i, (Integer) obj);
			} else if (obj.getClass().equals(Long.class)) {
				stmt.setLong(i, (Long) obj);
			} else if (obj.getClass().equals(String.class)) {
				stmt.setString(i, (String) obj);
			} else if (obj.getClass().equals(Date.class)) {
				stmt.setDate(i, (Date) obj);
			} else {
				throw new SQLException("Unknown class: " + obj.getClass().getName()); //$NON-NLS-1$
			}
		}

		if (stmt.execute()) {
			ret = stmt.getResultSet();
		} else {
			return Optional.empty();
		}

		return Optional.ofNullable(ret);
	}

	public static String sqlString(String a) {
		try {
			Optional<ResultSet> opt = sql(a);
			if (!opt.isPresent())
				return "Done."; //$NON-NLS-1$
			return opt.<String>map((rs) -> {
				StringBuilder sb = new StringBuilder();
				sb.append("RESULT:\n"); //$NON-NLS-1$
				try {
					ArrayList<Integer> cols = new ArrayList<>();
					ResultSetMetaData rsmd = rs.getMetaData();
					for (int i = 0; i < rsmd.getColumnCount(); i++)
						cols.add(rsmd.getColumnType(i + 1));
					while (rs.next()) {
						sb.append("\n"); //$NON-NLS-1$
						for (int i = 0; i < rsmd.getColumnCount(); i++) {
							sb.append(rs.getString(i + 1));
							sb.append(", "); //$NON-NLS-1$
						}
					}
					rs.close();
				} catch (SQLException e) {
					sb.append("\n\nERROR:" + MiscUtil.exception(e)); //$NON-NLS-1$
				}
				return sb.toString();
			}).get();
		} catch (Exception e) {
			return MiscUtil.exception(e);
		}
	}

	public static String toSqlArray(ArrayList<Integer> a) {
		if (a == null || a.size() == 0)
			return "'{}'"; //$NON-NLS-1$
		StringBuilder sb = new StringBuilder();
		sb.append("'{"); //$NON-NLS-1$
		for (int i = 0; i < a.size(); i++) {
			sb.append(a.get(i));
			if (i < a.size() - 1)
				sb.append(", "); //$NON-NLS-1$
			else
				sb.append("}'"); //$NON-NLS-1$
		}
		return sb.toString();
	}

	public static String toSqlString(String s) {
		return (s == null) ? "NULL" : ("'" + s.replaceAll("'", "''") + "'"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
	}

}

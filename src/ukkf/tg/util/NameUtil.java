package ukkf.tg.util;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChat;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class NameUtil {
    public static String getChatName(Chat chat) {
        String ret = chat.getId().toString();
        if (chat.isUserChat().booleanValue()) {
            ret = String.valueOf(ret) + " private " + chat.getFirstName(); //$NON-NLS-1$
            if (chat.getLastName() != null) {
                ret = String.valueOf(ret) + " " + chat.getLastName(); //$NON-NLS-1$
            }
            if (chat.getUserName() != null) {
                ret = String.valueOf(ret) + " @" + chat.getUserName(); //$NON-NLS-1$
            }
        } else if (chat.isGroupChat().booleanValue()) {
            ret = String.valueOf(ret) + " group " + chat.getTitle(); //$NON-NLS-1$
            if (chat.getUserName() != null) {
                ret = String.valueOf(ret) + " @" + chat.getUserName(); //$NON-NLS-1$
            }
        } else if (chat.isSuperGroupChat().booleanValue()) {
            ret = String.valueOf(ret) + " supergroup " + chat.getTitle(); //$NON-NLS-1$
            if (chat.getUserName() != null) {
                ret = String.valueOf(ret) + " @" + chat.getUserName(); //$NON-NLS-1$
            }
        }
        return ret;
    }
    
    public static String getName(Chat usr) {
        if (!usr.isUserChat())
            return "Someone"; //$NON-NLS-1$
        String ret = usr.getFirstName();
        if (usr.getLastName() != null)
            ret += " " + usr.getLastName(); //$NON-NLS-1$
        return ret;
    }
    
    public static String getName(User usr) {
        String ret = usr.getFirstName();
        if (usr.getLastName() != null) {
            ret = ret + " " + usr.getLastName(); //$NON-NLS-1$
        }
        return ret;
    }
    
    public static String getNameKnown(Integer id) throws Exception {
        String ret = SqlUtil.getString("stats", id, "name").orElse(""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        String last = SqlUtil.getString("stats", id, "last_name").orElse(""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        if (last.length() > 0)
            ret += " " + last; //$NON-NLS-1$
        return ret;
    }
    
    public static String getNameLong(Chat usr) {
        if (!usr.isUserChat())
            return "Someone"; //$NON-NLS-1$
        String ret = "Id: " + usr.getId(); //$NON-NLS-1$
        ret += "\nFirstName: " + usr.getFirstName(); //$NON-NLS-1$
        if (usr.getLastName() != null)
            ret += "\nLastName: " + usr.getLastName(); //$NON-NLS-1$
        if (usr.getUserName() != null)
            ret += "\nUserName: @" + usr.getUserName(); //$NON-NLS-1$
        return ret;
    }
    
    public static String getNameLong(User usr) {
        String ret = "Id: " + usr.getId(); //$NON-NLS-1$
        ret += "\nName: " + usr.getFirstName(); //$NON-NLS-1$
        if (usr.getLastName() != null) {
            ret += "\nLastName: " + usr.getLastName(); //$NON-NLS-1$
        }
        if (usr.getUserName() != null) {
            ret += "\nUsername: @" + usr.getUserName(); //$NON-NLS-1$
        }
        return ret;
    }
    
    public static String getNameLongKnown(Integer id) throws Exception {
        String ret = "Id: " + id.toString(); //$NON-NLS-1$
        ret += "\nName: " + SqlUtil.getString("stats", id, "name").orElse(""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        String tmp = SqlUtil.getString("stats", id, "last_name").orElse(""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        if (tmp.length() > 0)
            ret += "\nLastName: " + tmp; //$NON-NLS-1$
        tmp = SqlUtil.getString("stats", id, "username").orElse(""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        if (tmp.length() > 0)
            ret += "\nUsername: " + tmp; //$NON-NLS-1$
        return ret;
    }
    
    public static String getNameLongUnknown(Integer id, TelegramLongPollingBot bot) {
        GetChat get = new GetChat();
        get.setChatId(Long.valueOf(id));
        try {
            return NameUtil.getNameLong(bot.execute(get));
        } catch (TelegramApiException e) {
            ApiUtil.notifyException(bot, "", e); //$NON-NLS-1$
            return "blocked user"; //$NON-NLS-1$
        }
    }
    
    public static String getNameUnknown(Integer id, TelegramLongPollingBot bot) {
        GetChat get = new GetChat();
        get.setChatId(Long.valueOf(id));
        try {
            return NameUtil.getName(bot.execute(get));
        } catch (TelegramApiException e) {
            ApiUtil.notifyException(bot, "", e); //$NON-NLS-1$
            return "blocked user"; //$NON-NLS-1$
        }
    }
    
}

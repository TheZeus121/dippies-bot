package ukkf.tg.util;

import java.util.Optional;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChatMember;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.ChatMember;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import ukkf.tg.BotConfig;

public class ApiUtil {

    public static boolean isAdmin(long chatID, int userID, TelegramLongPollingBot bot) {
        GetChatMember gcm = new GetChatMember();
        gcm.setChatId(chatID);
        gcm.setUserId(userID);
        ChatMember cm;
        try {
            cm = bot.execute(gcm);
        } catch (TelegramApiException e) {
            notifyException(bot, "", e); //$NON-NLS-1$
            cm = null;
        }
        String status;
        if (cm == null)
            status = ""; //$NON-NLS-1$
        else
            status = cm.getStatus();
        return status.equalsIgnoreCase("administrator") || status.equalsIgnoreCase("creator"); //$NON-NLS-1$ //$NON-NLS-2$
    }

    public static Optional<Message> notifyException(TelegramLongPollingBot bot, String text, Throwable e) {
        StringBuilder sb = new StringBuilder();
        sb.append(text);
        sb.append("\n\nThis is sent with .notifyException\n\n"); //$NON-NLS-1$
        sb.append(MiscUtil.exception(e));
        return notifyMe(sb.toString(), bot);
    }

    public static Optional<Message> notifyMe(String smth, TelegramLongPollingBot bot) {
        SendMessage msg = new SendMessage();
        msg.setText(smth);
        msg.setChatId(Long.toString(BotConfig.DEV_GROUP));
        try {
            return Optional.ofNullable(bot.execute(msg));
        } catch (TelegramApiException e) {
            System.out.println("Error while notify me"); //$NON-NLS-1$
            e.printStackTrace();
            return Optional.empty();
        }
    }

}

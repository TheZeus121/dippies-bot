package ukkf.tg.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;

public class FileUtil {
    public static void writeToFile(File file, String str) throws IOException {
        BufferedOutputStream out = new BufferedOutputStream(Files.newOutputStream(file.toPath()));
        byte[] buf = str.getBytes();
        out.write(buf);
        out.close();
    }
    
    public static void downloadToFile(File file, String url) throws IOException {
        URL website = new URL(url);
        ReadableByteChannel rbc = Channels.newChannel(website.openStream());
        FileOutputStream fos = new FileOutputStream(file.getAbsolutePath());
        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        fos.close();
        rbc.close();
    }
    
    public static File downloadToNewFile(String url) throws IOException {
        return downloadToNewFile("dip", "bin"); //$NON-NLS-1$//$NON-NLS-2$
    }
    
    public static File downloadToNewFile(String url, String extension) throws IOException {
        File file = File.createTempFile("dip", "." + extension); //$NON-NLS-1$//$NON-NLS-2$
        file.deleteOnExit();
        downloadToFile(file, url);
        return file;
    }
}

package ukkf.tg.util;

import java.util.List;

import com.flickr4java.flickr.Flickr;

import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.PhotoSize;

public class MiscUtil {
    public static Flickr flickr;
    
    public static String exception(Throwable e) {
        StringBuilder sb = new StringBuilder();
        sb.append(e.getClass().getSimpleName());
        sb.append(" : "); //$NON-NLS-1$
        sb.append(e.getMessage());
        StackTraceElement[] stack_trace = e.getStackTrace();
        for (StackTraceElement element : stack_trace) {
            sb.append("\n"); //$NON-NLS-1$
            sb.append(element.getClassName());
            sb.append("#"); //$NON-NLS-1$
            sb.append(element.getMethodName());
            sb.append(" "); //$NON-NLS-1$
            sb.append(element.getFileName());
            sb.append(":"); //$NON-NLS-1$
            sb.append(element.getLineNumber());
            if (element.isNativeMethod())
                sb.append(" NATIVE"); //$NON-NLS-1$
        }
        return sb.toString();
    }
    
    public static PhotoSize getLargest(List<PhotoSize> list) {
        int w = 0;
        int i = -1;
        for (int j = 0; j < list.size(); j++) {
            if (list.get(j).getWidth() > w) {
                w = list.get(j).getWidth();
                i = j;
            }
        }
        return list.get(i);
    }
    
    public static boolean hasGIF(Message msg) {
        if (!msg.hasDocument())
            return false;
        return msg.getDocument().getMimeType().equals("video/mp4"); //$NON-NLS-1$
    }
}

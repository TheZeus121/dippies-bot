#!/bin/sh
heroku config:set BOT_VERSION=$(git describe --tags --long)
git push heroku --follow-tags
git push origin --follow-tags
